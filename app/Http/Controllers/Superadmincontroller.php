<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App\Product;
use App\Http\Requests;
use App\Http\Controllers\Controller;

session_start();
class Superadmincontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        
        $this->middleware('auth');
        $this->middleware('role:admin');

        //array index
         if (Session::all())
            {
                //$data = Session::all();

                //
            }else{
                Auth::logout();
            }    
    }
    public function index()
    {
        //
       // echo "This is super admin with id =".$_SESSION['userid'].'\n'.$_SESSION['username'].'\n'.$_SESSION['useremail'].'\n'.$_SESSION['created_at'];
         $products = \DB::table('products')->paginate(10);
         //$forums = \DB::table('forums')->get();
         //$users = \DB::table('user_profile')->paginate(5);
         $product=1;
         return view('superadmin.index',compact('products','product'));
    }


    public function forum()
    {
        $forums = \DB::table('forums')->paginate(15);
        return view('superadmin.index',compact('forums'));
    }

    public function user()
    {
        $users = \DB::table('user_profile')->paginate(5);
        return view('superadmin.index',compact('users'));
    }

    public function table()
    {
        $tables = \DB::table('user_profile')->paginate(5);
        return view('superadmin.index',compact('tables'));
    }
    public function delete(Request $request)
    {
        $user_id=$request->user_id;
        if($user_id == $_SESSION['userid'])
        {
            \Flash::success('Please Select Different user to Delete');
            return redirect('adminuser');
        }else {
        $tables = \DB::table('user_profile')->where('user_id', '=', $user_id)->delete();
        \Flash::success(' User Deleted Successfully');
            return redirect('adminuser');
        }
    }
    public function deleteproduct(Request $request)
    {
        $product_id=$request->product_id;
        $tables = \DB::table('products')->where('id', '=', $product_id)->delete();
        \Flash::success('Product Deleted Successfully');
            return redirect('adminproduct');
        
    }
    public function deleteforum(Request $request)
    {
        $forum_id=$request->forum_id;
        $tables = \DB::table('forums')->where('id', '=', $forum_id)->delete();
        \Flash::success('Forum Post Deleted Successfully');
            return redirect('adminforum');
    }
    public function superadminlogin(Request $request)
    {
        $email=$request->email;
        $pwd=$request->pwd;
        if( $email == "programmeratalberich@outlook.com" && $pwd == "developer@alberich")
        {
             $tables = \DB::table('user_profile')->paginate(5);
             $admincontrol="1";
              //return redirect('admintable')->with('admincontrol', $admincontrol);
            return view('superadmin.index',compact('tables','admincontrol'));
        }else {
            \Flash::error('Your Not a Valid User To login');
            return back()->withInput();
        }
    }


    public function deleteproducttable()
    {
        $tables = \DB::table('products')->delete();
        $admincontrol="1";
        \Flash::success('Product Table Deleted Successfully');
           // return back()->withInput();
        return redirect('admintable');
    }

    public function deleteforumtable()
    {
        $tables = \DB::table('forums')->delete();
        $admincontrol="1";
        \Flash::success('Forum  Table Deleted Successfully');
           // return back()->withInput();
        return redirect('admintable');
    }
    public function deleteforumcommenttable()
    {
        $tables = \DB::table('forum_comments')->delete();
        $admincontrol="1";
        \Flash::success('Forum Comment Table Deleted Successfully');
           // return back()->withInput();
        return redirect('admintable');
    }
    public function deleteuserprofiletable()
    {
        $tables = \DB::table('user_profile')->delete();
        $admincontrol="1";
        \Flash::success('User Profile Table Deleted Successfully');
            //return back()->withInput();
        return redirect('admintable');
    }

    public function truncateproducttable()
    {
        $tables = \DB::table('products')->truncate();
        $admincontrol="1";
        \Flash::success('Forum Post Deleted Successfully');
           // return back()->withInput();
        return redirect('admintable');
    }

    public function truncateforumtable()
    {
        $tables = \DB::table('forums')->truncate();
        $admincontrol="1";
        \Flash::success('Forum Post Deleted Successfully');
           // return back()->withInput();
        return redirect('admintable');
    }
    public function truncateforumcommenttable()
    {
        $tables = \DB::table('forum_comments')->truncate();
        $admincontrol="1";
        \Flash::success('Forum Post Deleted Successfully');
            //return back()->withInput();
        return redirect('admintable');
    }
    public function truncateuserprofiletable()
    {
        $tables = \DB::table('user_profile')->truncate();
        $admincontrol="1";
        \Flash::success('Forum Post Deleted Successfully');
            //return back()->withInput();
        return redirect('admintable');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
