<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Product;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use App\Http\Controllers\Redirect;
use Session;
use Auth;


session_start();
class ProductsController extends Controller
{

    public function __construct()
    {
        
        $this->middleware('auth');
        $this->middleware('role:admin');
        if (Session::has('flash_notification.message'))
       {
        Session::forget('flash_notification.message'); 
                session()->forget('flash_notification.message');
       }
        //array index
         if (Session::all())
            {
                //$data = Session::all();

                //
            }else{
                Auth::logout();
            }    
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->my_uri();
         $key= $_SESSION['key'];
         $_SESSION['userid']= Auth::user()->id;
        $_SESSION['q'] = $request->get('q');
        $products = Product::where('language', '=', $_SESSION['key'])
            ->where(function($query) {
                /** @var $query Illuminate\Database\Query\Builder  */
                $q=$_SESSION['q'];
                return $query->where('name', 'LIKE', '%'.$q.'%')
                    ->orWhere('model', 'LIKE', '%'.$q.'%');
            })
            ->orderBy('name')->paginate(15);
       return view('products.index', compact('products', $_SESSION['q'],$_SESSION['userid']));
    }

    
    
    
     public function englishtofrenchadmin(Request $request)
    {
        $this->my_uri();
         $key= $_SESSION['key'];
        $_SESSION['q'] = $request->get('q');
        $products = Product::where('language', '=', $_SESSION['key'])
            ->where(function($query) {
                /** @var $query Illuminate\Database\Query\Builder  */
                $q=$_SESSION['q'];
                return $query->where('name', 'LIKE', '%'.$q.'%')
                    ->orWhere('model', 'LIKE', '%'.$q.'%');
            })
            ->orderBy('name')->paginate(15);
        return view('products.englishtofrenchadmin', compact('products', $_SESSION['q']));
    }
    
     
    
     public function englishtoitalianadmin(Request $request)
    {
        $this->my_uri();
         $key= $_SESSION['key'];
        $_SESSION['q'] = $request->get('q');
        $products = Product::where('language', '=', $_SESSION['key'])
            ->where(function($query) {
                /** @var $query Illuminate\Database\Query\Builder  */
                $q=$_SESSION['q'];
                return $query->where('name', 'LIKE', '%'.$q.'%')
                    ->orWhere('model', 'LIKE', '%'.$q.'%');
            })
            ->orderBy('name')->paginate(15);
        return view('products.englishtoitalianadmin', compact('products', $_SESSION['q']));
    }
    
    
    
 
 
 public function englishtospanishadmin(Request $request)
    {
        //echo $request;
        $this->my_uri();
         $key= $_SESSION['key'];
        $_SESSION['q'] = $request->get('q');
        $products = Product::where('language', '=', $_SESSION['key'])
            ->where(function($query) {
                /** @var $query Illuminate\Database\Query\Builder  */
                $q=$_SESSION['q'];
                return $query->where('name', 'LIKE', '%'.$q.'%')
                    ->orWhere('model', 'LIKE', '%'.$q.'%');
            })
            ->orderBy('name')->paginate(15);
        return view('products.englishtospanishadmin', compact('products', $_SESSION['q']));
    }
    public function englishtohindhiadmin(Request $request)
    {
        $this->my_uri();
        $_SESSION['key'] =4;
         $key= $_SESSION['key'];
        $_SESSION['q'] = $request->get('q');
        $products = Product::where('language', '=', $_SESSION['key'])
            ->where(function($query) {
                /** @var $query Illuminate\Database\Query\Builder  */
                $q=$_SESSION['q'];
                return $query->where('name', 'LIKE', '%'.$q.'%')
                    ->orWhere('model', 'LIKE', '%'.$q.'%');
            })
            ->orderBy('name')->paginate(15);
        return view('products.englishtohindhiadmin', compact('products', $_SESSION['q']));
    }
    public function englishtosanskritadmin(Request $request)
    {
       $this->my_uri();
        $_SESSION['key'] =5;
         $key= $_SESSION['key'];
        $_SESSION['q'] = $request->get('q');
        $products = Product::where('language', '=', $_SESSION['key'])
            ->where(function($query) {
                /** @var $query Illuminate\Database\Query\Builder  */
                $q=$_SESSION['q'];
                return $query->where('name', 'LIKE', '%'.$q.'%')
                    ->orWhere('model', 'LIKE', '%'.$q.'%');
            })
            ->orderBy('name')->paginate(15);
        return view('products.englishtosanskritadmin', compact('products', $_SESSION['q']));
    }
    public function englishtopunjabadmin(Request $request)
    {
        $this->my_uri();
        $_SESSION['key'] =6;
         $key= $_SESSION['key'];
        $_SESSION['q'] = $request->get('q');
        $products = Product::where('language', '=', $_SESSION['key'])
            ->where(function($query) {
                /** @var $query Illuminate\Database\Query\Builder  */
                $q=$_SESSION['q'];
                return $query->where('name', 'LIKE', '%'.$q.'%')
                    ->orWhere('model', 'LIKE', '%'.$q.'%');
            })
            ->orderBy('name')->paginate(15);
        return view('products.englishtopunjabadmin', compact('products', $_SESSION['q']));
    }

    public function englishtomarathiadmin(Request $request)
    {
        $this->my_uri();
        $_SESSION['key'] =7 ;
         $key= $_SESSION['key'];
        $_SESSION['q'] = $request->get('q');
        $products = Product::where('language', '=', $_SESSION['key'])
            ->where(function($query) {
                /** @var $query Illuminate\Database\Query\Builder  */
                $q=$_SESSION['q'];
                return $query->where('name', 'LIKE', '%'.$q.'%')
                    ->orWhere('model', 'LIKE', '%'.$q.'%');
            })
            ->orderBy('name')->paginate(15);
        return view('products.englishtomarathiadmin', compact('products', $_SESSION['q']));
    }

    public function englishtobengaladmin(Request $request)
    {
        $this->my_uri();
        $_SESSION['key'] =8;
         $key= $_SESSION['key'];
        $_SESSION['q'] = $request->get('q');
        $products = Product::where('language', '=', $_SESSION['key'])
            ->where(function($query) {
                /** @var $query Illuminate\Database\Query\Builder  */
                $q=$_SESSION['q'];
                return $query->where('name', 'LIKE', '%'.$q.'%')
                    ->orWhere('model', 'LIKE', '%'.$q.'%');
            })
            ->orderBy('name')->paginate(15);
        return view('products.englishtobengaladmin', compact('products', $_SESSION['q']));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $key=$_SESSION['key'];
        return view('products.create',compact('key'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'model' => 'required',  
            'language' =>'required',  
        ]);
        $key=$_SESSION['key'];
        //echo $data['user_id']=$_SESSION['userid'];
        
        $data = $request->only('name', 'model','language','user_id');
         //print_r($data);
        if($this->my_validate($data) < 1 )
        {
                $product = Product::create($data);
            \Flash::success($product->name . ' Saved Successfully');
            if($key == 0)
            {
                //echo "products";
                return redirect('products');
            }elseif($key == 1)
            {
                //echo "englishtofrenchadmin";
                return redirect('englishtofrenchadmin');
            }elseif($key == 2)
            {
                //echo "englishtoitalianadmin";
                return redirect('englishtoitalianadmin');
            }elseif($key == 3)
            {
                //echo "englishtospanishadmin";
                return redirect('englishtospanishadmin');
            }else{
                return redirect('products');
            }
        }else{
            \Flash::error('Language Already Saved! Please Try Another');
            return back()->withInput();
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $key=$_SESSION['key'];        
        //echo $id;
        $product = Product::findOrFail($id);
        return view('products.edit', compact('product','key'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product = Product::findOrFail($id);
        $this->validate($request, [
            'name' => 'required',
            'model' => 'required',
            'photo' => 'mimes:jpeg,png|max:10240'
        ]);
        $data = $request->only('name', 'model');

        if ($request->hasFile('photo')) {
            $data['photo'] = $this->savePhoto($request->file('photo'));
            // I'm not deleting old photo, as stub image file is used by multiple product.
        }

        $product->update($data);
        if (count($request->get('category_lists')) > 0) {
            $product->categories()->sync($request->get('category_lists'));
        } else {
            // no category set, detach all
            $product->categories()->detach();
        }

        \Flash::success($product->name . ' Updated Successfully.');
        $key=$_SESSION['key'];
        if($key == 0)
            {
                //echo "products";
                return redirect('products');
            }elseif($key == 1)
            {
                //echo "englishtofrenchadmin";
                return redirect('englishtofrenchadmin');
            }elseif($key == 2)
            {
                //echo "englishtoitalianadmin";
                return redirect('englishtoitalianadmin');
            }elseif($key == 3)
            {
                //echo "englishtospanishadmin";
                return redirect('englishtospanishadmin');
            }else{
                return redirect('products');
            }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {


        Product::find($id)->delete();
        \Flash::success('Product deleted successfully.');
        return back()->withInput();
        // switch ($_SESSION['key']) {
        // return redirect()->action('ProductsController@index', [1]);
        //     case 0:
        //          return redirect()->route('products.englishtofrench');
        //         break;
        //     case 1:
        //         return redirect()->route('products.englishtofrench');
        //         break;
        //     case 2:
        //          return redirect()->route('products.englishtofrench');
        //         break;
        //     case 3:
        //         return redirect()->route('products.englishtofrench');
        //         break;
        //     default:
        //          return redirect()->route('products.index');
        //         break;
        // }
        
    }

    /**
     * Move uploaded photo to public/img folder
     * @param  UploadedFile $photo
     * @return string
     */
    protected function savePhoto(UploadedFile $photo)
    {
        $fileName = str_random(40) . '.' . $photo->guessClientExtension();
        $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'img';
        $photo->move($destinationPath, $fileName);
        return $fileName;
    }
    // 
    // Get the Url Segment
    // 
    public function my_uri()
    {
        $segments = explode('/', trim(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH), '/'));
        $numSegments = count($segments); 
        $currentSegment = $segments[$numSegments - 1];
        if($currentSegment == 'englishtospanishadmin')
        {
             $key= 3;
            $_SESSION['key']=$key;
        }elseif($currentSegment == 'englishtofrenchadmin')
        {
             $key=1;
            $_SESSION['key']=$key;
        }
        elseif($currentSegment == 'englishtoitalianadmin' )
        {
            $key=2;
            $_SESSION['key']=$key;
        }
        else
        {
            $key=0;
            $_SESSION['key']=$key;
        } 
    }
public function show()
    {   
        $key=$_SESSION['key']; 
        $q=$_GET['q'];
        $_SESSION['q']=$q;
        $products = Product::where('language', '=', $_SESSION['key'])
            ->where(function($query) {
                /** @var $query Illuminate\Database\Query\Builder  */
                $q=$_SESSION['q'];
                return $query->where('name', 'LIKE', '%'.$q.'%')
                    ->orWhere('model', 'LIKE', '%'.$q.'%');
            })
            ->orderBy('name')->paginate(15);
            return view('products.show', compact('products','q'));
    }

    public function my_validate($data)
    {
        $name = $data['name'];
        $model=$data['model'];
        $language=$data['language'];
       //////////////New Based On Req/////////////
         $users=Product::Where('model', $model)
            ->Where('name', $name)
            ->Where ('language',$language)
            ->get();
        $count = $users->count();
       ///////////////////////////////////////////
        

        ////////////Old Workin ////////////
        //  $users=Product::Where('model', $model)
        //     ->Where ('language',$language)
        //     ->get();
        // $count = $users->count();
        ///////////// ///////////////////


        //echo $count;
        return $count;
        // if($count == 1 || $count > 0)
        // {
        //     //echo "Model Or Language Already Added";
        // }else
        // {
        //     //echo "Ready To Add New Model";
        // }
}   
    public function user()
    {
        # code...
        echo "UserSubmit";
    }


}
