<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Product;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Illuminate\Support\Facades\Input;
use Image;
use Validator;
use Response;
use Redirect;
use DB;
use App\Forum;
use App\Forum_comment;
use App\User;

session_start();
class UserController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:admin');
        //echo $_SESSION['key'];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      if(isset($_SESSION['username']))
      {
        $name=$_SESSION['username'];
      }else
      {
        \Flash::info('Session Time out Log in to view your profile ');
        return view('products.index1', compact('products', $_SESSION['q']));
      }
        $name=$_SESSION['username'];
        $userid=$_SESSION['userid'];
        $email=$_SESSION['useremail'];
        $created_at=$_SESSION['created_at'];
        $updated_at=$_SESSION['created_at'];

    // if ($_SESSION['username'] instanceof ErrorException) {
      
    //     \Flash::info('Session Time out Log in to view your profile ');
    //     return response()->view('products.index', [], 500);
    // }
    

         /////////////----------------  Contribution--------------///////////////
   $my_count =$this->contribution();
   $forum_count=$this->forum_contribution($userid);
   $forum_cmt_count=$this->forum_cmt_contribution($userid);
    //print_r($my_count);
   // $users=Product::Where('user_id',$_SESSION['userid'])
   //          ->Where ('language','1')
   //          ->get();
   //  $count = $users->count();
   /////////////----------------  Contribution--------------///////////////

     $user_info = \DB::table('user_profile')->where('user_id',$_SESSION['userid'])->get();
      if (count($user_info) >= 1)
      {
        return view('products.userajaxregister',compact('user_info','count','my_count','forum_count','forum_cmt_count'));
      }else{
         $add_new =\DB::table('user_profile')->insert([
          ['user_id' => $userid, 'userName' => $name,'email'=>$email,'created_at'=>$created_at,'updated_at'=>$updated_at]
      ]);
         $user_info = \DB::table('user_profile')->where('user_id',$_SESSION['userid'])->get();
         return view('products.userajaxregister',compact('user_info','count','my_count','forum_count','forum_cmt_count'));
      }
    }
    public function userprofile(Request $request)
    {
      //$userName=$_SESSION['username'];
      $my_count =$this->contribution();
      $forum_count=$this->forum_contribution($_SESSION['userid']);
      $forum_cmt_count=$this->forum_cmt_contribution($_SESSION['userid']);
    $user_info = \DB::table('user_profile')->where('user_id',$_SESSION['userid'])->get();
    return view('products.userajaxregister',compact('user_info','my_count','forum_count','forum_cmt_count'));
        //echo "ur Looking for user profile";
        //echo $_SESSION['userid'].$_SESSION['username'].$_SESSION['useremail'];
    }
    public function testing(Request $request)
    {
      if (!isset($_SESSION['key'])) {
          \Flash::info('Session Time out Log in to view your profile ');
           return redirect('/usertest');  
        }

        $edit = $request->edit;
        $edit="My Profile";
        $forum_count=$this->forum_contribution($_SESSION['userid']);
        $forum_cmt_count=$this->forum_cmt_contribution($_SESSION['userid']);
        $user_details = \DB::table('user_profile')->where('user_id',$_SESSION['userid'])->get();
        
      return view('products.userajaxregister', compact('edit',$_SESSION['key'],'user_details','forum_count','forum_cmt_count'));

        // if (isset($_SESSION['key'])) {

        //   # code...
        //   return view('products.userajaxregister', compact('edit',$_SESSION['key'],'user_details','forum_count'));
        // }
        // else
        // {
        //    \Flash::info('Session Time out Log in to view your profile ');
        //     return redirect('/usertest');
        // }
        //echo "return";
    }
    public function contribution()
    {
      $users=Product::Where('user_id',$_SESSION['userid'])
            ->Where ('language','0')
            ->get();
    $count['d'] = $users->count();
      $users=Product::Where('user_id',$_SESSION['userid'])
            ->Where ('language','1')
            ->get();
    $count['f'] = $users->count();
    $users=Product::Where('user_id',$_SESSION['userid'])
            ->Where ('language','2')
            ->get();
    $count['i'] = $users->count();
    $users=Product::Where('user_id',$_SESSION['userid'])
            ->Where ('language','3')
            ->get();
    $count['s'] = $users->count();

    $users=Product::Where('user_id',$_SESSION['userid'])
            ->get();
    foreach ($users as $key => $value) {
           $count['updated']=$value->updated_at;
        }

      return $count;

    }
    public function forum_contribution($user_id)
{
  $users=Forum::Where('user_id',$_SESSION['userid'])  
                ->get();
   $forum_count = $users->count();
   return $forum_count;
}
   public function forum_cmt_contribution()
{
  //echo $userid;
  //$user_id= 6;//$_SESSION['userid']
  $users=Forum_comment::Where('user_id',$_SESSION['userid'])  
                ->get();
   $forum_cmt_count = $users->count();
   //echo  $forum_cmt_count;
   return $forum_cmt_count;
}
    public function useradd(Request $request)
    {
      $validator = Validator::make($request->all(),[
            'name' => 'required|alpha|regex:/^[\pL\s\-]+$/u',
            'email' =>'required|email',
            'business' =>'active_url',
            'languages'=>'alpha|regex:/^[\pL\s\-]+$/u',
            'about'=>'',
            'contact'=>'numeric|min:10'
        ]);
      if ($validator->fails()) {
            $messages = $validator->messages();
            return back()->withErrors($validator)
                        ->withInput();
        }else{
           $user_id=$_SESSION['userid'];
           $name = $request->name;
           $email = $request->email;
           $business = $request->business;
           $laguages = $request->languages;
           $contact = $request->contact;
           $about = $request->about;
           $_SESSION['business']=$business;
           $_SESSION['laguages']=$laguages;
           $_SESSION['about']=$about;
           $_SESSION['contact']=$contact;
           if(strlen($contact) === 10 || $contact==='' )
           {

           }else
           {
            \Flash::error('Contact Number Must Be 10 Numbers');
            return back()->with($business,$laguages)->withInput();
           }
/////////////////// User email And User name Validation in User Table ///////////////// 

          $users=User::Where('email', $email)
            
            ->get();

        $count = $users->count();
        //echo $count;
        if($count > 1)
        {
          \Flash::error('Email Already Taken Use Different mail -id ');
          
            return back()->withInput(); 
        }else {

        }
/////////////////// User email And User name Validation in User Table /////////////////
        //If Image Not Uploded
 if($request->file_source == '')
 {
    //echo "no photo";
    $user_info = \DB::table('user_profile')->where('user_id',$_SESSION['userid'])->get();
         foreach($user_info as $index=>$user)
          {
            $img_name=$user->photo;
         } 
    $forum_count=$this->forum_contribution($_SESSION['userid']);
    $forum_cmt_count=$this->forum_cmt_contribution($_SESSION['userid']);
     $user_info=\DB::table('users')->where('id', $_SESSION['userid'])->update(array('name'=>$name,'email'=>$email));  
          $user_info=\DB::table('user_profile')->where('user_id', $_SESSION['userid'])->update(array('userName'=>$name,'email'=>$email,'photo'=>$img_name,'languageknown'=>$laguages,'businessinfo'=>$business,'contactinfo'=>$contact,'aboutme' => $about));
          $user_info = \DB::table('user_profile')->where('user_id',$_SESSION['userid'])->get();
          $my_count =$this->contribution();
          \Flash::success('User Profile Updated successfully.');
          $msg="myModal";
          $model="modal";
          return view('products.userajaxregister',compact('user_info','my_count','msg','model','forum_count','forum_cmt_count'));
            
 }
 else{
        $image = $request->file_source;
        $img_name=$_FILES['file_source']['name'];
        $valid_exts = array('jpeg', 'jpg', 'png', 'gif');
        // thumbnail sizes
        $sizes = array(100 => 100);

        $ext = strtolower(pathinfo($_FILES['file_source']['name'], PATHINFO_EXTENSION));
        if (in_array($ext, $valid_exts)) {
          /* resize image */
          foreach ($sizes as $w => $h) {
            //$files[] = resize($w, $h);
            $files[]=$this->resize($w,$h);


          }
          /////////////////////////////  Update Goes Here //////////////////////////////       
          $user_info=\DB::table('users')->where('id', $_SESSION['userid'])->update(array('name'=>$name,'email'=>$email));  
          $user_info=\DB::table('user_profile')->where('user_id', $_SESSION['userid'])->update(array('userName'=>$name,'email'=>$email,'photo'=>$img_name,'languageknown'=>$laguages,'businessinfo'=>$business,'contactinfo'=>$contact,'aboutme' => $about));
          $user_info = \DB::table('user_profile')->where('user_id',$_SESSION['userid'])->get();
          $my_count =$this->contribution();
          $forum_count=$this->forum_contribution($_SESSION['userid']);
          \Flash::success('User Profile Updated successfully.');
          return view('products.userajaxregister',compact('user_info','my_count','forum_count','forum_cmt_count'));

          /////////////////////////////  Update Goes Here //////////////////////////////   

        } else {
          $msg = 'Unsupported file';
        }
 }
}
    }
  public  function resize($width, $height){
  /* Get original image x y*/
  list($w, $h) = getimagesize($_FILES['file_source']['tmp_name']);
  /* calculate new image size with ratio */
  $ratio = max($width/$w, $height/$h);
  $h = ceil($height / $ratio);
  $x = ($w - $width / $ratio) / 2;
  $w = ceil($width / $ratio);
  /* new file name */
  $path = 'public/img/profile/'.$_FILES['file_source']['name'];
  /* read binary data from image file */
  $imgString = file_get_contents($_FILES['file_source']['tmp_name']);
  /* create image from string */
  $image = imagecreatefromstring($imgString);
  $tmp = imagecreatetruecolor($width, $height);
  imagecopyresampled($tmp, $image,
    0, 0,
    $x, 0,
    $width, $height,
    $w, $h);
  /* Save image */
  switch ($_FILES['file_source']['type']) {
    case 'image/jpeg':
      imagejpeg($tmp, $path, 100);
      break;
    case 'image/png':
      imagepng($tmp, $path, 0);
      break;
    case 'image/gif':
      imagegif($tmp, $path);
      break;
    default:
      exit;
      break;
  }
  return $path;
  /* cleanup memory */
  imagedestroy($image);
  imagedestroy($tmp);
}
public function usertest()
{

  $dt = new \DateTime();
  
  $user_info=\DB::table('user_profile')->where('user_id', $_SESSION['userid'])->update(array('updated_at'=>$dt));
  session()->flush();
  return Redirect::to(url('logout'));
  ////return redirect()->action('logout');
}

public function userdemo()
{

  return view('autocomplete');
  ////return redirect()->action('logout');
}
public function remove_img()
{

//
$user_info=\DB::table('user_profile')->where('user_id', $_SESSION['userid'])->get();  
foreach ($user_info as $key => $value) {
  $img=$value->photo;
}
if($img == '')
{
  
  $my_count =$this->contribution();
  $forum_count=$this->forum_contribution($_SESSION['userid']);
  $forum_cmt_count=$this->forum_cmt_contribution($_SESSION['userid']);
  $user_info = \DB::table('user_profile')->where('user_id',$_SESSION['userid'])->get();
  \Flash::error('No User Profile Image Selected To Delete.');
  return view('products.userajaxregister',compact('user_info','my_count','forum_count','forum_cmt_count'));
}else{

  //echo "update photo";
  $user_info=\DB::table('user_profile')->where('user_id', $_SESSION['userid'])->update(array('photo'=>''));
  $my_count =$this->contribution();
  $forum_count=$this->forum_contribution($_SESSION['userid']);
  $forum_cmt_count=$this->forum_cmt_contribution($_SESSION['userid']);
  $user_info = \DB::table('user_profile')->where('user_id',$_SESSION['userid'])->get();
  \Flash::success('User Profile Image Removed successfully.');
  return view('products.userajaxregister',compact('user_info','my_count','forum_count','forum_cmt_count'));

}

}

public function autocomplete(){
  //$term = Input::get('term');
  
  //$results = array();
  
  // $queries = DB::table('users')
  //   ->where('first_name', 'LIKE', '%'.$term.'%')
  //   ->orWhere('last_name', 'LIKE', '%'.$term.'%')
  //   ->take(5)->get();
  
  // foreach ($queries as $query)
  // {
      
  //     //$results[] = [ 'id' => $query->id, 'value' => $query->first_name.' '.$query->last_name ];
  // }
 // $results[] = [ 'id' => 1, 'value' => 'saleem'.' '.'saif' ];
//return Response::json($results);
}

}
