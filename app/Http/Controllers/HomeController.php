<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Product;
use App\Forum;
use Session;
use App\Forum_comment;
use App\Http\Controllers\Redirect;

session_start();
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['only'=>'index']);
        if (Session::has('flash_notification.message'))
       {
        Session::forget('flash_notification.message'); 
                session()->forget('flash_notification.message');
       }
    }

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function index()
    {
        return view('home');
    }

    public function welcome()
    {
        return view('welcome');
    }
    public function aboutus()
    {
        return view('aboutus');
    }

    public function contactus()
    {
        return view('contactus');
    }

    public function blog()
    {
        return view('blog');
    }
    public function index1(Request $request)
    {
        $key=0;
        $_SESSION['key']=$key;
        $products = \DB::table('products')
                ->Where ('language','0')
                ->orderBy('name')->paginate(15);
        return view('products.index1', compact('products',$_SESSION['key']));
    }
    public function englishtofrench(Request $request)
    {
        $key=1;
        $_SESSION['key']=$key;
    $products = \DB::table('products')
                ->Where ('language','1')
                ->orderBy('name')->paginate(15);
        return view('products.englishtofrench', compact('products',$_SESSION['key']));
    }
    public function englishtoitalian(Request $request)
    {
        $key=2;
        $_SESSION['key']=$key;
    $products = \DB::table('products')
                ->Where ('language','2')
                ->orderBy('name')->paginate(15);
        return view('products.englishtoitalian', compact('products',$_SESSION['key']));
    }
    public function englishtospanish(Request $request)
    {
        $key=3;
        $_SESSION['key']=$key;
        $products = \DB::table('products')
                ->Where ('language','3')
                ->orderBy('name')->paginate(15);
        //print_r($products);
                //echo $_SESSION['key'];
        return view('products.englishtospanish', compact('products',$_SESSION['key']));
    }
    public function englishtohindhi(Request $request)
    {
        $key=4;
        $_SESSION['key']=$key;
        $products = \DB::table('products')
                ->Where ('language','3')
                ->orderBy('name')->paginate(15);
        //print_r($products);
                //echo $_SESSION['key'];
        return view('products.englishtohindhi', compact('products',$_SESSION['key']));
    }
    public function englishtosanskrit(Request $request)
    {
        $key=5;
        $_SESSION['key']=$key;
        $products = \DB::table('products')
                ->Where ('language','3')
                ->orderBy('name')->paginate(15);
        //print_r($products);
                //echo $_SESSION['key'];
        return view('products.englishtosanskrit', compact('products',$_SESSION['key']));
    }
    public function englishtopunjab(Request $request)
    {
        $key=6;
        $_SESSION['key']=$key;
        $products = \DB::table('products')
                ->Where ('language','3')
                ->orderBy('name')->paginate(15);
        //print_r($products);
                //echo $_SESSION['key'];
        return view('products.englishtopunjab', compact('products',$_SESSION['key']));
    }
     public function englishtomarathi(Request $request)
    {
        $key=7;
        $_SESSION['key']=$key;
        $products = \DB::table('products')
                ->Where ('language','3')
                ->orderBy('name')->paginate(15);
        //print_r($products);
                //echo $_SESSION['key'];
        return view('products.englishtomarathi', compact('products',$_SESSION['key']));
    }
     public function englishtobengal(Request $request)
    {
        $key=8;
        $_SESSION['key']=$key;
        $products = \DB::table('products')
                ->Where ('language','3')
                ->orderBy('name')->paginate(15);
        //print_r($products);
                //echo $_SESSION['key'];
        return view('products.englishtobengal', compact('products',$_SESSION['key']));
    }

    public function showuser()
    {   
        //$_SESSION['key']=$key;
        
        $q=$_GET['q'];
        //$_SESSION['q']=$q;
        $key=$_GET['key'];
        $_SESSION['key']=$key;
         //echo $_SESSION['key'];

        //echo $q . '' . $key;
        
        $products = Product::where('language', '=', $_SESSION['key'])
            ->where(function($query) {
                /** @var $query Illuminate\Database\Query\Builder  */
                $q=$_GET['q'];
                return $query->where('name', 'LIKE', '%'.$q.'%')
                    ->orWhere('model', 'LIKE', '%'.$q.'%');
            })
            ->orderBy('name')->paginate(15);
            //return redirect('userregister');
            return view('products.showuser', compact('products','q'));
    }
    public function profiles()
    {
        $users = \DB::table('user_profile')->paginate(5);
        //var_dump($users);
        //$message="View Users Profile";
        //var_dump($users);
        return view('products.profiles',compact('users'));
    }
    public function viewprofile()
    {
         $id=$_GET['id'];
         //echo $id;
        //echo "demo".$id;
        $users = \DB::table('user_profile')->where('user_id',$id)->get();
        foreach ($users as $key => $value) {
           $user_id=$value->user_id;
        }
        // echo $user_id;
         $user_id=$id;
        $my_count =$this->contribution($user_id);
        //print_r($my_count);
        $forum_count=$this->forum_contribution($user_id);
        $forum_cmt_count=$this->forum_cmt_contribution($user_id);

        //echo $user_id;
        // $my_count =$this->contribution($id);
        // //print_r($my_count);
        // $forum_count=$this->forum_contribution($id);

        return view('products.viewprofile',compact('users','my_count','forum_count','forum_cmt_count')) ;  
    }
    public function contribution($user_id)
    {
      $users=Product::Where('user_id',$user_id)
            ->Where ('language','0')
            ->get();
    $count['d'] = $users->count();
      $users=Product::Where('user_id',$user_id)
            ->Where ('language','1')
            ->get();
    $count['f'] = $users->count();
    $users=Product::Where('user_id',$user_id)
            ->Where ('language','2')
            ->get();
    $count['i'] = $users->count();
    $users=Product::Where('user_id',$user_id)
            ->Where ('language','3')
            ->get();
    $count['s'] = $users->count();
    //print_r($count);
      return $count;

    }
public function productdetails()
{
  $id=$_GET['id'];
  $rid=$_GET['rid'];
  //echo $rid."user id ".$id;
  
  $user_info=\DB::table('user_profile')->where('user_id',$id)->get();

   if(count($user_info) >= 1 ){
    foreach ($user_info as $key => $value) {
     # code...
        $user_id=$value->user_id;
        $user_name=$value->userName;
        $user_email=$value->email;
   }
   $user_info=array('user_id'=>$user_id,'user_name'=>$user_name,'user_email'=>$user_email);
   }else
   {
    $user_info=array('user_id'=>" ",'user_name'=>" ",'user_email'=>" ");
   }
//print_r($user_info);

$row_info=\DB::table('products')->where('id',$rid)->get();
  
return view('products.modeldetails',compact('user_info','row_info'));
}
public function forum_contribution($user_id)
{
  
  $users=Forum::Where('user_id',$user_id)  
                ->get();
   $forum_count = $users->count();
   return $forum_count;
}
 public function forum_cmt_contribution($user_id)
{
  //$user_id= 6;//$_SESSION['userid']
    // echo $user_id;
  $users=Forum_comment::Where('user_id',$user_id)  
                ->get();
   $forum_cmt_count = $users->count();
   //echo  $forum_cmt_count;
   return $forum_cmt_count;
}

}
