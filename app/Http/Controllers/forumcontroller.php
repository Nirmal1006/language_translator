<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Forum;
use App\ViewSearch;
use App\Forum_comment;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Validator;
use Session;
use Illuminate\Foundation\Validation\ValidatesRequests;


session_start();
class forumcontroller extends Controller
{
     public function __construct()
    {
       $this->check_session_key();
       // $_SESSION['key'];
       if (Session::has('flash_notification.message'))
       {
        Session::forget('flash_notification.message'); 
                session()->forget('flash_notification.message');
       }
    }
  /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        switch ($_SESSION['key']) {
            case 0:
                $_SESSION['lan']="German";
                echo "German"; 
                break;
            case 1:
                $_SESSION['lan']="Frence";
                echo "Frence"; 
                break;
            case 2:
                $_SESSION['lan']="Italian";
                echo "Italian"; 
                break;
            case 3:
                $_SESSION['lan']="Spanish";
                echo "Spanish";
                break;
            default:
                $_SESSION['lan']="German";
                echo "German- Default"; 
                break;
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('products.addpost');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $validator = Validator::make($request->all(),[
            'name' => 'required|regex:/^[\pL\s\-]+$/u' 
            ]);
         if ($validator->fails()) {
            $messages = $validator->messages();
            \Flash::error('User Name Must be alphabets only');
            return back()->withErrors($validator)
                        ->withInput();
        }else{
         $input = Input::all();
        //print_r($input);
         $forum = Forum::create($input);
         // 
         $forum_id=\DB::table('forums')->max('id');
         $add_new =\DB::table('forum_comments')->insert([
          ['user_id' => 1, 'forum_id' => $forum_id]]);
         \Flash::success('Post Added Successfully');
            return redirect('forumsearch');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        
        $name=$request->name;
        $language_id=$request->language_id;
        $heading=$request->heading;
        $email=$request->email;
        $user_id=$request->user_id;
        $forum_id=$request->forum_id;
        $description=$request->description;
        //$dt = new \DateTime();
        $user_info=\DB::table('forums')
                   ->where('id', $forum_id)
                   ->update(array('name'=>$name,'email'=>$email,'heading'=>$heading,'description'=>$description));
        if($user_info == TRUE)
        {
            \Flash::success('Post Updated Successfully');
        $forum = Forum::where('language_id', '=', $_SESSION['key'])->paginate(4);
        return view('products.forumsearch',compact('forum'));
        }else
        {
            \Flash::error('Update failed Please Try again');
            return back()->withInput();
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function search()
    {
        # code...
        
        $forum = Forum::where('language_id', '=', $_SESSION['key'])->paginate(4);
        //var_dump($forum);
        return view('products.forumsearch',compact('forum'));
    }
    public function dosearch(Request $request)
    {
        $search=1;
        Session::put('my_search', '1');
        $q = $request->q; 
        $_SESSION['q']=trim($q);
        $q=$_SESSION['q'];
        //echo $q;
        if($q == '')
        {
         return back()->withInput();   
        } else{ 
         $forum =\DB::table('view_searchs')
            ->where('language_id', '=', $_SESSION['key'])
            ->where(function($query) {
                /** @var $query Illuminate\Database\Query\Builder  */
                $q=$_SESSION['q'];
                return $query->where('name', 'LIKE', '%'.$q.'%')
                    ->orWhere('heading', 'LIKE', '%'.$q.'%')
                    ->orWhere('description', 'LIKE', '%'.$q.'%')
                    ->orWhere('email', 'LIKE', '%'.$q.'%')
                    ->orWhere('cmtName', 'LIKE', '%'.$q.'%')
                    ->orWhere('cmtEmail', 'LIKE', '%'.$q.'%')
                    ->orWhere('comment', 'LIKE', '%'.$q.'%');
            })->orderBy('forum_id')->paginate(4);
        
          
        return view('products.forumsearch2',compact('forum','search','q','count'));
       
        }     
    }
    public function postcomment()
    {
         
        //echo $_SESSION['user_id'];
             $forum_id=$_GET['id'];
             $_SESSION['forum_id']=$_GET['id'];
          
        $forum = Forum::where('language_id', '=', $_SESSION['key'])->where('id', '=', $_SESSION['forum_id'])->get(); 
         $forum_comments =  Forum_comment::where('forum_id',$forum_id)->orderBy('id', 'DESC')->paginate(2);
        
         return view('products.fpostcomment',compact('forum','forum_comments'));
    }
    public function faddcomments(Request $request)
    {
        # code...
        //$input = Input::all();
        $name = $request->name;
        $email = $request->email;
        $comment = $request->comment;
        $forum_id = $request->forum_id;
        if(isset($_SESSION['forum_id'])){
           $forum_id = $_SESSION['forum_id'];
       }else{
            $forum_id = $request->forum_id;
       }
        //$_SESSION['forum_id']=$request->forum_id;
        $user_id=$request->user_id;

         $forum =\DB::table('forum_comments')->insert([
          ['name' => $name, 'forum_id' => $forum_id,'user_id'=>$user_id,'email'=>$email,'comment'=>$comment]
      ]);


        //$forum = Forum_comment::create($input);


        $forum = Forum::where('language_id', '=', $_SESSION['key'])->where('id', '=',$forum_id)->get(); 


        //$forum_comments = \DB::table('forum_comments')->where('forum_id',$forum_id)->get();


        $forum_comments =  Forum_comment::where('forum_id', '=',  $forum_id)->get();
         
         //\Flash::success('Comments Posted Successfully');
         //

         //var_dump($forum);
         

         \Flash::success('Comment Added Successfully');
         //return view('products.forumsearch2',compact('forum','forum_comments'));

         //
         //return view('products.forumsearch2',compact('forum','forum_comments'));
         //return view('products.forumsearch2',compact('forum','forum_comments'));
         return back()->with('forum',$forum)->with('forum_comments',$forum_comments)->withInput();
           

            //return redirect('fpostcomment',compact('forum'));
    }
    public function check_session_key()
    {
        # code...
        if(isset($_SESSION['key']))
        {
            return $_SESSION['key'];
        }else{
            \Flash::success('Session TimeOut Your Redirect To HomePage');
            $_SESSION['key'] = 0;
            return $_SESSION['key'];
        }
    }
    public function editforumpost(Request $request)
    {
       //$input = Input::all();
       //print_r($input);
        $forum_id = $request->forum_id;
        $user_id = $request->user_id;
       $forum = Forum::where('language_id', '=', $_SESSION['key'])->where('id', '=', $forum_id)->get();
       //print_r($forum); 
       // Check Valid User Or Not //
       foreach ($forum as $key => $value) {
            $check_user_id =$value->user_id;
            $forum_id=$value->id;
       }
       // echo $user_id;
       // echo $check_user_id;
       if($user_id == $check_user_id)
       {
         //Do Edit
            //$forum_id="Do Edit";
            //$edit="doedit";
          return view('products.editforumpost',compact('forum','forum_id'));
       }else 
       {
        // Return Back 
        \Flash::error('You Dont Have Permission to edit this post');
        return back()->withInput();
       }
    }
    public function editforumcomment(Request $request)
    {
        # code...
        $comment_id = $request->comment_id;
        $forum_id = $request->forum_id;
        $forum_comments =  Forum_comment::where('id', '=',  $comment_id)->where('user_id','=',$_SESSION['userid'])->get();
        $forum = Forum::where('id', '=', $forum_id)->get();
        //print_r($forum_comments);
        return view('products.editfcomment',compact('forum','forum_comments'));
    }
    public function forumpostcommentupdate(Request $request)
    {
        # code...
        $cmt_id = $request->comment_id;
        $comment = $request->comment;
        if($cmt_id == "" || $comment == "")
        {
            \Flash::error('Please Enter Comment Before You Submit');
            return back()->withInput();
        }else{
        //$_SESSION['forum_id']=$request->forum_id;
         $user_id=$_SESSION['userid'];
         $user_info=\DB::table('forum_comments')->where('id', $cmt_id)->where('user_id',$user_id)->update(array('comment'=>$comment));
        
         $forum = Forum::where('language_id', '=', $_SESSION['key'])->paginate(4);
        
         //\Flash::success('Comment Updated Successfully');
         //return back()->withInput();
         return redirect('forumsearch')->with('success', 'Comment Updated Successfully');
         //return view('products.forumsearch',compact('forum'));
        }
    }
    public function singlepost(Request $request)
    {
        # code...
        $id=$_GET['id'];
        $forum = Forum::where('language_id', '=', $_SESSION['key'])->where('id', $id)->get();
        //var_dump($forum);
        return view('products.singleforumpost',compact('forum'));
    }
    public function selectlan(Request $request)
    {
      $select_lan =Input::get('lan');
      if($select_lan == 1)
      {
        $lan =1;
      }elseif($select_lan == 2)
      {
        $lan =2;
      }
      elseif($select_lan == 3)
      {
        $lan =3;
      }else{
        $lan =1;
      }
      //echo $lan;
      $forum = Forum::where('language_id', '=', $_SESSION['key'])->paginate(4);
        //var_dump($forum);
        return view('products.forumsearch',compact('forum','lan'));

    }
    public function forumenglish()
    {
        # code...
        $lan=1;
        $my_lan="english";
        $forum = Forum::where('language_id', '=', $_SESSION['key'])->paginate(4);
        //var_dump($forum);
        return view('products.forumsearch',compact('forum','lan','my_lan'));

    }
    public function forumtamil()
    {
        # code...
        $lan=2;
        $my_lan="Tamil";
        $forum = Forum::where('language_id', '=', $_SESSION['key'])->paginate(4);
        //var_dump($forum);
        return view('products.forumsearch',compact('forum','lan','my_lan'));

    }
    public function updateforumpost(Request $request)
    {
       //  $input = Input::all();
       // print_r($input);
        $forum_id=$request->forum_id;
        $user_id=$request->user_id;
        $heading=$request->heading;
        $description=$request->description;
        if($heading == " " || $description == " ")
        {
            \Flash::error('Enter Some content to Update');
            return back()->withInput();
        }else
        {
            $user_info=\DB::table('forums')
                   ->where('id', $forum_id)
                   ->update(array('heading'=>$heading,'description'=>$description,));
        if($user_info == TRUE)
        {
            \Flash::success('Post Updated Successfully');
        $forum = Forum::where('language_id', '=', $_SESSION['key'])->paginate(4);
        return redirect('forumsearch')->with('success', 'Comment Updated Successfully');;
        //return view('products.forumsearch',compact('forum'));
        }else
        {
            \Flash::error('Update failed Please Try again');
            return back()->withInput();
        }
        }
    }

    public function like(Request $request)
    {
        $ip= $this->get_client_ip_env();
        $comment_id=$request->comment_id;
        $user_id=$request->user_id;
        $like=1;
        $dislike=0;
        $comments=\DB::table('comments')->where('forum_comment_id',$comment_id)
        ->count();        
        if($comments == 0)
        {
            $add_like=\DB::table('comments')->insert([['forum_comment_id' => $comment_id, 'ip' => $ip,'like'=>$like,'dislike'=>$dislike,'user_id' => $user_id]]);
            $forum = Forum::where('language_id', '=', $_SESSION['key'])->paginate(4);
            return redirect('forumsearch')->with('forum', $forum);
         }else {
           $check=\DB::table('comments')->where('forum_comment_id',$comment_id)->get();
            foreach ($check as $key => $value) {
               
                 if($value->user_id == $user_id )
                 {
                    
            $forum = Forum::where('language_id', '=', $_SESSION['key'])->paginate(4);
            return redirect('forumsearch')->with('forum', $forum);
                 }else{ 
                   
            $add_like=\DB::table('comments')->insert([['forum_comment_id' => $comment_id, 'ip' => $ip,'like'=>$like,'dislike'=>$dislike,'user_id' => $user_id]]);
            $forum = Forum::where('language_id', '=', $_SESSION['key'])->paginate(4);
            return redirect('forumsearch')->with('forum', $forum);        
                 }     
            }
         }
    }
    public function dislike(Request $request)
    {
       $ip= $this->get_client_ip_env();
        $comment_id=$request->comment_id;
        $user_id=$request->user_id;
        $like=0;
        $dislike=1;
        $comments=\DB::table('comments')->where('forum_comment_id',$comment_id)
        ->count();        
        if($comments == 0)
        {
            $add_like=\DB::table('comments')->insert([['forum_comment_id' => $comment_id, 'ip' => $ip,'like'=>$like,'dislike'=>$dislike,'user_id' => $user_id]]);
            $forum = Forum::where('language_id', '=', $_SESSION['key'])->paginate(4);
            return redirect('forumsearch')->with('forum', $forum);
         }else {
           $check=\DB::table('comments')->where('forum_comment_id',$comment_id)->get();
            foreach ($check as $key => $value) {
                 
                 if($value->user_id == $user_id  )
                 {
            $forum = Forum::where('language_id', '=', $_SESSION['key'])->paginate(4);
            return redirect('forumsearch')->with('forum', $forum);
                 }else{ 
            $add_like=\DB::table('comments')->insert([['forum_comment_id' => $comment_id, 'ip' => $ip,'like'=>$like,'dislike'=>$dislike,'user_id' => $user_id]]);
            $forum = Forum::where('language_id', '=', $_SESSION['key'])->paginate(4);
            return redirect('forumsearch')->with('forum', $forum);        
                 }     
            }
         }
    }
        // Function to get the client ip address
    public function get_client_ip_env() {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if(getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if(getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if(getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if(getenv('HTTP_FORWARDED'))
            $ipaddress = getenv('HTTP_FORWARDED');
        else if(getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';

        return $ipaddress;
    }

}



 
       


 // if($comments === 0)
 //        {
 //            $add_like=\DB::table('comments')->insert([['forum_comment_id' => $comment_id, 'ip' => $ip,'like'=>$like]]);
 //            $like_count=\DB::table('comments')->where('forum_comment_id',$comment_id)
 //            ->where('like',$like)->count();
 //            $forum = Forum::where('language_id', '=', $_SESSION['key'])->paginate(4);
 //            return redirect('forumsearch')->with('forum', $forum);
 //        }else{
 //            $check=\DB::table('comments')->where('forum_comment_id',$comment_id)->get();
 //            foreach ($check as $key => $value) {
 //                $db_ip=$value->ip;
 //                $like=$value->like;
 //                if($db_ip === $ip && $like == 1)
 //                {
 //            $like_count=\DB::table('comments')->where('forum_comment_id',$comment_id)
 //            ->where('like',$like)->count();
 //            $forum = Forum::where('language_id', '=', $_SESSION['key'])->paginate(4);
 //            return redirect('forumsearch')->with('forum', $forum);
 //                }else
 //                {

 //        $add_like=\DB::table('comments')->insert([['forum_comment_id' => $comment_id, 'ip' => $ip,'like'=>$like]]);
 //            $like_count=\DB::table('comments')->where('forum_comment_id',$comment_id)
 //            ->where('like',$like)->count();
 //            $forum = Forum::where('language_id', '=', $_SESSION['key'])->paginate(4);
 //            return redirect('forumsearch')->with('forum', $forum);
 //                }
 //            }
 //        }


















// // /////////////////////////////////////////////////////////
//  $forum_search =\DB::table('forums')
//             ->where('language_id', '=', $_SESSION['key'])
//             ->where(function($query) {
//                 /** @var $query Illuminate\Database\Query\Builder  */
//                 $q=$_SESSION['q'];
//                 return $query->where('name', 'LIKE', '%'.$q.'%')
//                     ->orWhere('heading', 'LIKE', '%'.$q.'%')
//                     ->orWhere('description', 'LIKE', '%'.$q.'%')
//                     ->orWhere('email', 'LIKE', '%'.$q.'%');
//             })->orderBy('name')->count();
//         //echo $forum_search;
//         if($forum_search > 0)
//         {
//             $forum=\DB::table('forums')
//             ->where('language_id', '=', $_SESSION['key'])
//             ->where(function($query) {
//                 /** @var $query Illuminate\Database\Query\Builder  */
//                 $q=$_SESSION['q'];
//                 return $query->where('name', 'LIKE', '%'.$q.'%')
//                     ->orWhere('heading', 'LIKE', '%'.$q.'%')
//                     ->orWhere('description', 'LIKE', '%'.$q.'%')
//                     ->orWhere('email', 'LIKE', '%'.$q.'%');
                    
//             })->orderBy('name')->paginate(4);
//             return view('products.forumsearch',compact('forum','search'));
//         }else
//         {
//             $forum=\DB::table('forums')
//             ->where('language_id', '=', $_SESSION['key'])
//             ->where(function($query) {
//                 /** @var $query Illuminate\Database\Query\Builder  */
//                 $q=$_SESSION['q'];
//                 return $query->where('name', 'LIKE', '%'.$q.'%')
//                     ->orWhere('heading', 'LIKE', '%'.$q.'%')
//                     ->orWhere('description', 'LIKE', '%'.$q.'%')
//                     ->orWhere('email', 'LIKE', '%'.$q.'%');
                    
//             })->orderBy('name')->paginate(4);
//             $forum_search_result =\DB::table('view_searchs')
//             ->where('language_id', '=', $_SESSION['key'])
//             ->where(function($query) {
//                 /** @var $query Illuminate\Database\Query\Builder  */
//                 $q=$_SESSION['q'];
//                 return $query->where('name', 'LIKE', '%'.$q.'%')
//                     ->orWhere('heading', 'LIKE', '%'.$q.'%')
//                     ->orWhere('description', 'LIKE', '%'.$q.'%')
//                     ->orWhere('email', 'LIKE', '%'.$q.'%')
//                     ->orWhere('cmtName', 'LIKE', '%'.$q.'%')
//                     ->orWhere('cmtEmail', 'LIKE', '%'.$q.'%')
//                     ->orWhere('comment', 'LIKE', '%'.$q.'%');
//             })->orderBy('name')->paginate(4);
//         return view('products.forumsearch',compact('forum_search_result','forum','search'));
//         }  