<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => 'web','auth'], function () {
    Route::get('/', 'HomeController@index1');
//     Route::get('/', function () {
//        $auth= $this->middleware('auth');
//        $auth_2= $this->middleware('role:admin');
//     if($auth || $auth_2)
//     {
//          //return redirect('/products');
//         return 'yes';
//     }else{
//          //return redirect('/userhome');
//         return 'no';
//     }
// });
    Route::auth();
    
    Route::get('/userhome', 'HomeController@index1');
    Route::get('/products', 'ProductsController@index');
    Route::get('/home', 'ProductsController@index');
    Route::get('/userregister', 'UserController@index');
    Route::get('/submituser', 'UserController@userprofile');
    Route::get('/userprofile', 'UserController@userprofile');
    Route::get('/aboutus', 'HomeController@aboutus');
    Route::get('/contactus', 'HomeController@contactus');
    Route::get('/blog', 'HomeController@blog');
    Route::get('/userprofilesearch', 'HomeController@viewprofile');
    Route::resource('products', 'ProductsController');
    Route::get('catalogs', 'CatalogsController@index');
    Route::resource('categories', 'CategoriesController');


   Route::resource('/englishtofrench', 'HomeController@englishtofrench');
    Route::resource('/englishtoitalian', 'HomeController@englishtoitalian');
    Route::resource('/englishtospanish', 'HomeController@englishtospanish');

    Route::resource('/englishtohindhi', 'HomeController@englishtohindhi');
    Route::resource('/englishtosanskrit', 'HomeController@englishtosanskrit');
     Route::resource('/englishtopunjab', 'HomeController@englishtopunjab');
     Route::resource('/englishtomarathi', 'HomeController@englishtomarathi');
     Route::resource('/englishtobengal', 'HomeController@englishtobengal');



    Route::resource('/show', 'ProductsController@show');
    Route::resource('/showuser', 'HomeController@showuser');
    Route::resource('/profiles', 'HomeController@profiles');
    Route::resource('/product', 'HomeController@index1');
    Route::resource('/user', 'UserController@useradd');
    
    Route::resource('/mytest', 'HomeController@viewprofile');

Route::resource('/remove_img', 'UserController@remove_img');

Route::resource('/productdetails', 'HomeController@productdetails');

Route::resource('/demo', 'HomeController@forum_contribution');
    Route::resource('/testing', 'UserController@testing');
    Route::resource('/userprofile', 'UserController@userprofile');

    Route::resource('/usertest', 'UserController@usertest');

    Route::resource('/englishtofrenchadmin', 'ProductsController@englishtofrenchadmin');
    Route::resource('/englishtoitalianadmin', 'ProductsController@englishtoitalianadmin');
    Route::resource('/englishtospanishadmin', 'ProductsController@englishtospanishadmin');

    Route::resource('/englishtohindhiadmin', 'ProductsController@englishtohindhiadmin');
    Route::resource('/englishtosanskritadmin', 'ProductsController@englishtosanskritadmin');
     Route::resource('/englishtopunjabadmin', 'ProductsController@englishtopunjabadmin');

     Route::resource('/englishtomarathiadmin', 'ProductsController@englishtomarathiadmin');

     Route::resource('/englishtobengaladmin', 'ProductsController@englishtobengaladmin');

    //      
    // Forum Controller    
    // 
    //Route::get('/forum', 'forumcontroller@index');
    Route::resource('/newpost', 'forumcontroller@create');
    Route::resource('/addpost', 'forumcontroller@store');
    Route::resource('/forumsearch', 'forumcontroller@search');
    Route::resource('/forumpostcomment', 'forumcontroller@postcomment');
    Route::resource('/fpostcomment', 'forumcontroller@faddcomments');
    Route::resource('/forumpost', 'forumcontroller@singlepost');
    Route::resource('/dosearch', 'forumcontroller@dosearch');
    Route::resource('/editforumpost', 'forumcontroller@editforumpost');
    Route::post('/forumsearch', 'forumcontroller@update');
    Route::resource('/editforumcomment', 'forumcontroller@editforumcomment');
    Route::resource('/fpostcommentupdate', 'forumcontroller@forumpostcommentupdate');
    Route::resource('/selectlan', 'forumcontroller@selectlan');
    Route::resource('/forumenglish', 'forumcontroller@forumenglish');
    Route::resource('/forumtamil', 'forumcontroller@forumtamil');
    Route::resource('/updateforumpost', 'forumcontroller@updateforumpost');


    Route::resource('like', 'forumcontroller@like');
     Route::resource('dislike', 'forumcontroller@dislike');

//     Route::resource('/dosearch', function () { updateforumpost
//         if(isset($q = $request->q; ))
//         {

//         }else{

//         }
// });
// --------------- Super Admin ----------- //  /
  
    Route::resource('/superadmin', 'Superadmincontroller@index');

    Route::resource('/adminproduct', 'Superadmincontroller@index');

    Route::resource('/adminforum', 'Superadmincontroller@forum');

    Route::resource('/adminuser', 'Superadmincontroller@user');

    Route::resource('/admintable', 'Superadmincontroller@table');

    Route::resource('/adminuserdelete', 'Superadmincontroller@delete');

    Route::resource('/adminproductdelete', 'Superadmincontroller@deleteproduct');

    Route::resource('/adminforumdelete', 'Superadmincontroller@deleteforum');

    Route::resource('/superadminlogin', 'Superadmincontroller@superadminlogin');

    Route::resource('/deleteproducttable', 'Superadmincontroller@deleteproducttable');
    Route::resource('/deleteforumtable', 'Superadmincontroller@deleteforumtable');
    Route::resource('/deleteforumcommenttable', 'Superadmincontroller@deleteforumcommenttable');
Route::resource('/deleteuserprofiletable', 'Superadmincontroller@deleteuserprofiletable');

    Route::resource('/truncateproducttable', 'Superadmincontroller@truncateproducttable');
    Route::resource('/truncateforumtable', 'Superadmincontroller@truncateforumtable');
    Route::resource('/truncateforumcommenttable', 'Superadmincontroller@truncateforumcommenttable');
    Route::resource('/truncateuserprofiletable', 'Superadmincontroller@truncateuserprofiletable');


     Route::resource('/demo', 'Superadmincontroller@testing');

// --------------- Super Admin ----------- //  
});
// Route::filter('/demo', function()superadmin
// {
//     if (Auth::guest()) return Redirect::guest(URL::route('account-sign-in'));
// });