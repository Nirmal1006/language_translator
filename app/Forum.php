<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Forum extends Model
{
    //
   protected $fillable = ['language_id', 'name', 'heading', 'user_id','email', 'description', 'created_at', 'updated_at'];

}
