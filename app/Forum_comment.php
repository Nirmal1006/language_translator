<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Forum_comment extends Model
{
    //
   protected $fillable = ['forum_id', 'name', 'email','user_id','comment', 'created_at', 'updated_at'];
}
