<div class="container">
	<div class="row">
	<div class="panel panel-info">
				  <div class="panel-heading">
				    	<h3 class="panel-title">Forum : <?php switch ($_SESSION['key']) {
            case 0:
                
                echo "German"; 
                break;
            case 1:
                
                echo "Frence"; 
                break;
            case 2:
               
                echo "Italian"; 
                break;
            case 3:
                
                echo "Spanish";
                break;
            default:
               
                echo "German- Default"; 
                break;
        } ?> </h3>
				  </div>
	</div><!---Header Over-->
	<hr>
	<div class="col-md-12">
	<div class="col-md-offset-4">
	{{ Form::open( [ 'url' => 'dosearch', 'class'=>'form-inline','method' => 'post', 'files' => true ] ) }}
		<!-- <form class="form-inline" role="form"> -->
    <div class="form-group">
     {!! Form::text('q', isset($q) ? $q : null, ['class'=>'form-control', 'placeholder' => 'Enter the word to search']) !!}
                    {!! $errors->first('q', '<p class="help-block">:message</p>') !!}
      <!-- <input type="q" class="form-control" id="q" placeholder="Search Here.."> -->
    </div>
    <button type="submit" class="btn fa fa-search btn-search"></button>
  <!-- </form> -->
  {!! Form::close() !!}
  
  </div>
	</div>
     &nbsp;
	<div class="col-md-12">
	
      <div class="table-responsive">          
        <table class="table table-bordered table-hover">
        <?php $seg=readsegment(); $sno=$seg*4+1; ?>
        @foreach ($forum as $key=>$forums)
        <?php $time=$forums->created_at;
				  if($forums->user_id === 0){
				  	   $user="Demo User";
				  	}else{
				  		$user=username($forums->user_id); }?>
    <thead>
      <tr>
        <th>Post No:&nbsp;&nbsp;{{$sno}}	</th>
        <th>Title : {{$forums->heading}}	</th>
        <?php if($forums->user_id === 0){
			 ?>
        <th>User Name: {{$forums->name}}	</th>
        <?php } else{ ?>
        	<th>User Name:<a href="/userprofilesearch/?id=<?php echo $user['id']; ?>"><?php echo $user['username']; ?></a>	</th>
        <?php } ?>	
        <th>Posted On: <?php echo date('M / j / Y g:i A', strtotime($time)); ?></th>
        <tr> <td colspan="4"><span style="font-size:22px;">Description :</span><p style="font-size:18px;padding-left:5em;">{{  $forums->description}}</p></td> </tr>
      </tr>

    </thead>
    <tbody>
    <tr>
	  <td colspan="4"><span style="font-size:22px;">Comments :</span><p style="font-size:18px;padding-left:5em;"></p>
	<!-- My Single Comment -->
<?php 
	$single_comment =get_single_comment($forums->id); 
	foreach ($single_comment as $key => $value) { ?>
		<?php if($value->user_id === 0){ ?>
		 <p>{{$value->name}}  - <span><i>Comment On</i>&nbsp;&nbsp;<?php echo date('M / j / Y g:i A', strtotime($value->created_at)); ?></span></p>
     <?php } else{ ?>
      <p><a href="/userprofilesearch/?id={{$value->user_id}}">{{$value->name}}</a> - <span><i>Comment On</i>&nbsp;&nbsp;<?php echo date('M / j / Y g:i A', strtotime($value->created_at)); ?></span></p>
      <?php } ?>
		<p>{{$value->comment}}</p>
		
	<!-- My Single Comment Close -->

		<hr> 
<?php 
	$id=$value->id;
	} ?>
<!-- Comment Text Post Here -->


	<!-- Comment From Here -->
	  </td>
	<!-- ////// Comment TD Close Here  -->
	</tr>

      <tr>
    
        <td colspan="4">
          <!-- Reply Comment Post  -->

<span class="pull-right">
<?php
if(isset($_SESSION['userid']))
  {$user_id = $_SESSION['userid'];}else{$user_id=0;}
 if(Auth::check() && isset($_SESSION['username'])) { ?>
  {!! Form::open(['url' => 'editforumpost', 'method'=>'post', 'class'=>'form-inline']) !!}
   <button type="submit" class="btn btn-primary fa fa-edit" aria-hidden="true">Edit Post</button>
   <input type="hidden" name="forum_id" id="forum_id" value="{{$forums->id}}">
    <input type="hidden" name="user_id" id="user_id" value="<?php echo $user_id; ?>">
  {!! Form::close() !!} 
<?php }else{ ?>
  <?php } ?>
  </span>
<span class="pull-right"> 
 <button data-toggle="collapse" data-target="#<?php echo $forums->id; ?>" class="btn btn-primary fa fa-reply" aria-hidden="true">&nbsp;&nbsp; Reply</button>
</span>
<!-- Reply Comment -->
<div id="<?php echo $forums->id; ?>" class="collapse">
{!! Form::open(['url' => 'fpostcomment', 'method'=>'post', 'class'=>'form-inline']) !!}
   <!--  <form class="form-inline" role="form"> -->

    <div class="form-group">
    	<?php if(Auth::check() && isset($_SESSION['username'])) { ?>
      <input type="text" class="form-control" id="text" name= "name" value="<?php echo $_SESSION['username']; ?>" readonly>
      <?php }else{ ?>
       <input type="text" class="form-control" id="text" name= "name" placeholder="Name" >
       
      	<?php }
if(isset($_SESSION['userid']))
	{$user_id = $_SESSION['userid'];}else{$user_id=0;}
		 ?>   
		<input type="hidden" name="forum_id" id="forum_id" value="{{$forums->id}}">
	   <input type="hidden" name="user_id" id="user_id" value="<?php echo $user_id; ?>">
    </div>
    <div class="form-group">
    <?php if(Auth::check() && isset($_SESSION['username'])) { ?>
				<input type="email" name="email" id="email" class="form-control form-control-lg" value="<?php echo $_SESSION['useremail']; ?> " readonly>
			<?php  }else { ?> 
				<input type="email" name="email" id="email" class="form-control form-control-lg" placeholder="Eamil">
			<?php } ?> 
     <!--  <input type="password" class="form-control" id="pwd" placeholder="Enter password"> -->
    </div>
        <div class="form-group">
      <textarea name="comment" id="comment" class="form-control" rows="2" cols="50" placeholer="Message"></textarea>
    </div>
    <button type="submit" class="btn btn-primary"><span class="fa fa-send">&nbsp;&nbsp;Post Comment</span></button>
 <!--  </form> -->
<!-- <a class="pull-right" href="" class="fa fa-comment" aria-hidden="true">&nbsp;&nbsp;Post Comment</a> -->

  {!! Form::close() !!} 
</div>
  </td>
 
      </tr>
    </tbody>
    @endforeach

  </table>
      </div>
{!! $forum->links() !!}
	</div>


	</div> <!---Row-->
 </div><!---Container-->


 @endsection
 <?php 
	
	function username($id)
	{
		$con=mysqli_connect("localhost","schanetz_schanet","schanetz2016","schanetz_translate");
		$sql="SELECT * FROM `user_profile` WHERE `user_id` = $id";
		$result = mysqli_query($con,$sql);

		if ($result->num_rows > 0) {
		  
		    while($row = $result->fetch_assoc()) {
		        //print_r($row);

		    //echo $row->username;
		        $user=$row['userName'];
		        $id = $row['id'];

		    }
		} else {
		    //$user=$id;	
		}
		$userdata = array('username' =>$user ,'id' =>$id );
		return $userdata;
	}
function readsegment()
    {         
    if(isset($_GET['page']))
    {
        $current_page=$_GET['page'];
    }else
    {
        $current_page=1;
    }
    $page=$current_page-1;
        return $page;
}
function get_single_comment($id){
	$single_comment= \DB::table('forum_comments')
                ->where('forum_id', '=', $id)
                ->orderby('created_at','DESC')
                ->get();
    return $single_comment;            
}
function get_comment($id){
$forum_comments= \DB::table('forum_comments')
                ->where('forum_id', '=', $id)
                ->get();
 return $forum_comments;
}
?>