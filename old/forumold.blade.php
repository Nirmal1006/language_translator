@extends('layouts.app')

@section('content')
<?php 
	
	function username($id)
	{
		$con=mysqli_connect("localhost","schanetz_schanet","schanetz2016","schanetz_translate");
		$sql="SELECT * FROM `user_profile` WHERE `user_id` = $id";
		$result = mysqli_query($con,$sql);

		if ($result->num_rows > 0) {
		  
		    while($row = $result->fetch_assoc()) {
		        //print_r($row);

		    //echo $row->username;
		        $user=$row['userName'];
		        $id = $row['id'];

		    }
		} else {
		    //$user=$id;	
		}
		$userdata = array('username' =>$user ,'id' =>$id );
		return $userdata;
	}
function readsegment()
    {         
    if(isset($_GET['page']))
    {
        $current_page=$_GET['page'];
    }else
    {
        $current_page=1;
    }
    $page=$current_page-1;
        return $page;
}
 ?>
<div class="container">
	<div class="row">
		<div class="panel panel-info">
				  <div class="panel-heading">
				    	<h3 class="panel-title">Forum : <?php switch ($_SESSION['key']) {
            case 0:
                
                echo "German"; 
                break;
            case 1:
                
                echo "Frence"; 
                break;
            case 2:
               
                echo "Italian"; 
                break;
            case 3:
                
                echo "Spanish";
                break;
            default:
               
                echo "German- Default"; 
                break;
        } ?> </h3>
				  </div>
				  <div class="panel-body">

				  {{ Form::open( [ 'url' => 'dosearch', 'class'=>'form-horizontal','method' => 'post', 'files' => true ] ) }}
						<div class="row">
            	<div class="col-md-3"></div>
            	<div class="col-md-4">
                <div class="form-group {!! $errors->has('q') ? 'has-error' : '' !!}">
                 
                    {!! Form::text('q', isset($q) ? $q : null, ['class'=>'form-control', 'placeholder' => 'Enter the word to search']) !!}
                    {!! $errors->first('q', '<p class="help-block">:message</p>') !!}
                </div>
                </div>
                <div class="col-md-2">
                <form>
                   <button type="submit" class="btn fa fa-search btn-search" >&nbsp;&nbsp;Search</button>
                   <br>  
                </form>
                </div>
                <div class="col-md-3"></div>
                </div>
		<?php $seg=readsegment(); $sno=$seg*4+1; ?>
				  {!! Form::close() !!}
				  @foreach ($forum as $key=>$forums)
				  <?php 
				  $time=$forums->created_at;
				  if($forums->user_id === 0){
				  	   $user="Demo User";
				  	}else{
				  		$user=username($forums->user_id);
				  		//print_r($user);
				  		} ?>
						<table class="table tabl-resposive table-bordered table-hover">
							<tr >
								<td style=" text-align: center;font-size:22px;">Post No:&nbsp;&nbsp;{{$sno}}</td>
								<td ><span style="font-size:22px;">Title :</span><span style="font-size:18px;">{{$forums->heading}}</span></td> 		
			<?php if($forums->user_id === 0){
			 ?>
			<td ><span style="font-size:22px;">User Name:&nbsp;&nbsp;</span><span style="font-size:18px;">{{$forums->name}}</span></td>
			<?php } else{ ?>
			<td ><span style="font-size:22px;">User Name:&nbsp;&nbsp;</span><span style="font-size:18px;"><a href="/userprofilesearch/?id=<?php echo $user['id']; ?>"><?php echo $user['username']; ?></a></span></td>
			<?php } ?>					

								<td ><span style="font-size:22px;">Posted On:&nbsp;&nbsp;</span><span style="font-size:18px;"><?php echo date('M / j / Y g:i A', strtotime($time)); ?> </span></td>
							</tr>
							<tr >
								<td colspan="4"><span style="font-size:22px;">Description :</span><p style="font-size:18px;padding-left:5em;">{{  $forums->description}}</p>
									<span class="pull-right">
										<a href="forumpostcomment?id=<?php echo $forums->id; ?>" class="fa fa-comment" aria-hidden="true">&nbsp;&nbsp;Post Comment</a>
 									</span>
								</td>
							</tr>
						</table>
						<hr>
							<?php $sno++; ?>
						@endforeach

						{!! $forum->links() !!}
					
				  </div>


		</div>
	</div>
</div>
 @endsection