<!--  -->
 {{ Form::open( [ 'url' => 'dosearch', 'class'=>'form-horizontal','method' => 'post', 'files' => true ] ) }}
						<div class="row">
            	<div class="col-md-3"></div>
            	<div class="col-md-4">
                <div class="form-group {!! $errors->has('q') ? 'has-error' : '' !!}">
                 
                    {!! Form::text('q', isset($q) ? $q : null, ['class'=>'form-control', 'placeholder' => 'Enter the word to search']) !!}
                    {!! $errors->first('q', '<p class="help-block">:message</p>') !!}
                </div>
                </div>
                <div class="col-md-2">
                <form>
                   <button type="submit" class="btn fa fa-search btn-search">&nbsp;&nbsp;Search</button>
                   <br>  
                </form>
                </div>
                <div class="col-md-3"></div>
                </div>
		<?php $seg=readsegment(); $sno=$seg*4+1; ?>
				  {!! Form::close() !!}
				  @foreach ($forum as $key=>$forums)
				  <?php 
				  $time=$forums->created_at;
				  if($forums->user_id === 0){
				  	   $user="Demo User";
				  	}else{
				  		$user=username($forums->user_id);
				  		//print_r($user);
				  		} ?>
				  		<div class="table-resposive ">
						<table class="table table-bordered table-hover">
							<tr >
								<td style=" text-align: center;font-size:22px;">Post No:&nbsp;&nbsp;{{$sno}}</td>
								<td ><span style="font-size:22px;">Title :</span><span style="font-size:18px;">{{$forums->heading}}</span></td> 		
			<?php if($forums->user_id === 0){
			 ?>
			<td ><span style="font-size:22px;">User Name:&nbsp;&nbsp;</span><span style="font-size:18px;">{{$forums->name}}</span></td>
			<?php } else{ ?>
			<td ><span style="font-size:22px;">User Name:&nbsp;&nbsp;</span><span style="font-size:18px;"><a href="/userprofilesearch/?id=<?php echo $user['id']; ?>"><?php echo $user['username']; ?></a></span></td>
			<?php } ?>					

								<td ><span style="font-size:22px;">Posted On:&nbsp;&nbsp;</span><span style="font-size:18px;"><?php echo date('M / j / Y g:i A', strtotime($time)); ?> </span></td>
							</tr>
							<tr >
								<td colspan="4"><span style="font-size:22px;">Description :</span><p style="font-size:18px;padding-left:5em;">{{  $forums->description}}</p>
									<span class="pull-right">
										<a href="forumpostcomment?id=<?php echo $forums->id; ?>" class="fa fa-comment" aria-hidden="true">&nbsp;&nbsp;Post Comment</a>
 									</span>
								</td>
							</tr>
							<tr>
								<td colspan="4"><span style="font-size:22px;">Comments :</span><p style="font-size:18px;padding-left:5em;"></p>
	<!-- Comment Text Post Here  -->
<div class="comments">
	<?php 
	$single_comment =get_single_comment($forums->id); 
	foreach ($single_comment as $key => $value) { ?>
		
		 <p>{{$value->name}}</p>
		<p>{{$value->comment}}</p>
		<span class="pull-right">	

 <button data-toggle="collapse" data-target="#<?php echo $forums->id; ?>" class="btn btn-primary fa fa-comment" aria-hidden="true">View More Comments</button>
</span>
		<hr> 

	<?php 
	$id=$value->id;
	} ?>



</div>

	<!-- Comment Text Post Here -->

<div id="<?php echo $forums->id; ?>" class="collapse">
<?php $user=get_comment($forums->id);
	//echo $user;
	//print_r($user);
	
	foreach ($user as $key => $value) { ?>
		<?php if ($id === $value->id ) {
			# code...
		}else{?>
		 <p>{{$value->name}}</p>
		<p>{{$value->comment}}</p>
		<hr> 

	<?php  }//echo $id;
	 } ?>
</div>
	<!-- Comment From Here -->
{!! Form::open(['url' => 'fpostcomment', 'method'=>'post', 'class'=>'form-inline col-lg']) !!}
<div class="container">
	<div class="row">
		<div class="form-group" class="form-inline">
			<div class="col-md-6">
			<?php if(Auth::check() && isset($_SESSION['username'])) { ?>
			<input type="text" class="form-control form-control-lg" name= "name"  id="name"  value="<?php echo $_SESSION['username']; ?>" readonly>
			<?php  }else { ?> 
			<input type="text" class="form-control form-control-lg" name= "name"  id="name" placeholder="Name" required>
			<?php 
if(isset($_SESSION['userid']))
	{$user_id = $_SESSION['userid'];}else{$user_id=0;}

			} ?>   
			<br>

			<input type="hidden" name="forum_id" id="forum_id" value="{{$forums->id}}">
			<input type="hidden" name="user_id" id="user_id" value="<?php echo $user_id; ?>">
			</div>
			<div class="col-md-6">
			<?php if(Auth::check() && isset($_SESSION['username'])) { ?>
				<input type="email" name="email" id="email" class="form-control form-control-lg" value="<?php echo $_SESSION['useremail']; ?> " readonly>
			<?php  }else { ?> 
				<input type="email" name="email" id="email" class="form-control form-control-lg" placeholder="Eamil">
			<?php } ?> 
			<br>
			</div>
			<br>
			<div class="col-md-12">
			<textarea name="comment" id="comment" class="form-control" cols="30" rows="6" placeholder="Comments" required></textarea>
			<br>	
			</div>
			<div class="col-md-12">
			<button class="btn btn-primary "> <span class="fa fa-send">&nbsp;&nbsp;Post</span></button>	
			</div>
		</div>
	</div>
</div>

			{!! Form::close() !!} 
	<!-- Comment From Here -->
						
								</td>	
							</tr>
						</table>
					</div>
						<hr>
							<?php $sno++; ?>
						@endforeach

						{!! $forum->links() !!}
					
				  </div>