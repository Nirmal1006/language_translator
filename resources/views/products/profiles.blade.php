@extends('layouts.app')

@section('content')
<style>
	.img-style img{
		width: 50px;
		height:50px;
	}
</style>
	<div class="container">
		<div class="row">
			<div class="panel panel-info">
				  <div class="panel-heading">
				    <h3 class="panel-title">User Profiles :</h3>
				  </div>
				  <div class="panel-body">
				    <div class="container">
						<div class="row">
							<div class="col-md-1"></div>
								<div class="col-md-10">
									<div class="row">
										<div class="col-md-6">	
										<table class="table table-hover">
									    <thead>
									      <tr>
									        <th>Name</th>
									        <th>Email</th>
									        <th>Image</th>
									      </tr>
									    </thead>
								    
    <tbody>
    @foreach($users as $index=>$user)
    <a href="/userprofilesearch/?id={{$user->id}}">	
	      <tr>
	        <td><a href="/userprofilesearch/?id={{$user->user_id}}">{{$user->userName}}</a></td>
	        <td><a href="/userprofilesearch/?id={{$user->user_id}}">{{$user->email}}</a></td>
	        <td><?php if(isset($user)){ ?> 
				<div class="img-style" >
				<?php if($user->photo == '') {  ?>
					<a href="/userprofilesearch/?id={{$user->user_id}}">
				{!! Html::image('public/img/profile/profile.jpg') !!}
					</a>
				<?php }else{ ?>
					<a href="/userprofilesearch/?id={{$user->user_id}}">
				{!! Html::image('public/img/profile/'.$user->photo) !!}
					</a>
				<?php }
				?>
				</div>
				<?php }else {} ?></td>
	      </tr>
     </a>
    @endforeach
    </tbody>
  </table>
  {!! $users->links() !!}
										</div>
										<div class="col-md-6"></div>
									</div>
								</div>
							<div class="col-md-1"></div>
						</div>	    	
				    </div>
				  </div>
			</div>
		</div>
	</div>
	
	
@endsection