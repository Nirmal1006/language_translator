<div class="form-group {!! $errors->has('name') ? 'has-error' : '' !!}">
  {!! Form::label('name', 'Name') !!}
  {!! Form::text('name', null, ['class'=>'form-control']) !!}
  {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {!! $errors->has('model') ? 'has-error' : '' !!}">
  {!! Form::label('model', 'Model') !!}
  {!! Form::text('model', null, ['class'=>'form-control']) !!}
  {!! $errors->first('model', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {!! $errors->has('language') ? 'has-error' : '' !!}">
  {!! Form::label('language', 'Model') !!}  
  {!! Form::text('language', 'example@gmail.com', ['class' => 'form-control']) !!}
  {!! $errors->first('model', '<p class="help-block">:message</p>') !!}
</div>
{!! Form::submit(isset($model) ? 'Update' : 'Save', ['class'=>'btn btn-primary']) !!}
