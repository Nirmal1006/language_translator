


<?php 
if(isset($_SESSION['business']))
{ 
$business=$_SESSION['business'];
$laguages=$_SESSION['laguages'];
$about=$_SESSION['about'];
$contact=$_SESSION['contact'];

}else{

}
//$m="confirm-delete";
 
	

?>
@extends('layouts.app')

@section('content')
<style>
	.img-style img{
		width: 100px;
		height:100px;
		margin-left: 50px;
		
	}
	.img-style-2 img{
		width: 50px;
		height:50px;
		margin-left: 70px;
		margin-top: -50px;
	}
</style>
<?php
if(isset($edit))
{
	$icon= "fa fa-user";
	$url="userprofile";
	$style="display:none";
} 
else
{
	$icon= "fa fa-pencil";
	$edit = "Edit";
	$url="testing";
	$style="";
}
if(isset($user_details)){
	//print_r($user_details);	

}else{

}
//print_r($my_count);

//echo $my_count;

?>

	<div class="container">
		<div class="row">
			<div >
		{{ Form::open( [ 'url' => $url, 'class'=>'form-horizontal','method' => 'post', 'files' => true ] ) }}
				<!-- <a href="/userregister" ><i class="fa fa-file-text ">&nbsp;&nbsp;My Profile</i></a> -->
				<button class="btn btn-primary" style="<?php echo $style; ?>"><i class="<?php echo $icon; ?>">&nbsp;&nbsp;<?php echo $edit; ?></i></button>	
				<input type="hidden" name="edit" id="edit" value="edit">	
							
		 {!! Form::close() !!}
			</div>
			<br>
			<!-- <a href="/userregister"><i class="fa fa-home">-Home</i></a>
			<a href="/testing"><i class="fa fa-edit">-Edit</i></a> -->
		</div>
	</div>
<?php 
	if($edit == 'Edit')
	{
		//echo "profile page"; 
		?>
<div class="container"> 
			<div class="row">
			@foreach($user_info as $index=>$user)
				<div class="col-md-6">
					<div class="my-style" style="padding:20px;">
					  	<div class="panel panel-warning">
										  <div class="panel-heading">
										    <h3 class="panel-title">Personal Information:</h3>
										  </div>
						</div>
						<table padding=2px  width="400px" style="margin-left:30px;"> 
							<tr>
						
							<td >Name : {{ $user->userName }} </td>
							<td rowspan="3">
								<div class="img-style" style="width:75px;height:75px;margin-left:125px;margin-top:-20px">	
								<?php if($user->photo == '') {  ?>
									{!! Html::image('public/img/profile/profile.jpg') !!}

								<?php }else{ ?>
									{!! Html::image('public/img/profile/'.$user->photo) !!}
								<?php }
									?>
									{{ Form::open( [ 'url' => 'remove_img', 'class'=>'form-horizontal','method' => 'post', 'files' => true ] ) }}
								<button class="btn btn-primary btn-xs pull-right"  >Remove Image</button>
								<input type="hidden" name="userid" id="userid" value="<?php $_SESSION['userid']; ?>">
								{!! Form::close() !!}
								</div>
								
							</td>
						</tr>

						<tr>
							<td>Email : {{ $user->email }} </td>
						</tr>

						<tr>
							<td>Languages Known : {{ $user->languageknown }}</td>
						</tr>
						</table>
						<br> 

						<div class="panel panel-warning">
										  <div class="panel-heading">
										    <h3 class="panel-title">About Me:</h3>
										  </div>
						</div>
						<p><?php echo $user->aboutme; ?></p>
					</div>
				</div>
				<div class="col-md-6">	
					<div class="table-style" style="padding:15px;"> 
						<div class=" panel-warning">
										  <div class="panel-heading">
										    <h3 class="panel-title">Business Details:</h3>
										  </div>
						</div>
						<br>
						<table padding=2px  width="400px" style="margin-left:30px">
							<tr> 
								<td>Business Information: {{ $user->businessinfo }}</td>
							</tr>
							<tr><td><br></td></tr>
							<tr> 
								<td>Contact Information:{{ $user->contactinfo }}</td>
							</tr>
							<tr><td><br></td></tr>
						</table>
						<div class="panel-warning">
										  <div class="panel-heading">
										    <h3 class="panel-title">Contributions:</h3>
										  </div>
						<div class="panel-body">
									    <div class="list-group">
				  <button type="button" class="list-group-item">Form Post's : <?php echo $forum_count; ?></button><?php if(isset($forum_cmt_count)){ $forum_cmt_count; }else{$forum_cmt_count=0;} ?>
				 <button type="button" class="list-group-item">Form Comment's : <?php echo $forum_cmt_count; ?></button>
				  <button type="button" class="list-group-item">Glossary(German): <?php echo $my_count['d']; ?></button>
				  <button type="button" class="list-group-item">Glossary(French): <?php echo $my_count['f']; ?></button>
				  <button type="button" class="list-group-item">Glossary(Italian): <?php echo $my_count['i']; ?></button>
				  <button type="button" class="list-group-item">Glossary(Spanish): <?php echo $my_count['s']; ?></button>
				  <button type="button" class="list-group-item">Active From :<?php echo date('M / j / Y g:i A', strtotime($user->created_at)); ?> </button>
				  <button type="button" class="list-group-item">Last Activity:<?php echo date('M / j / Y g:i A', strtotime($user->updated_at)); ?>
</button>
				</div>
						</div>
					</div> 
				</div>		
			</div>
		  @endforeach
		</div>
		
	<?php }else{ 
		//echo "edit Page";
		?>
<div class="container">
<div class="row">
		<div class="col-md-10 col-md-offset-1">
            <div class="panel panel-primary">
                <div class="panel-heading">User Profile Update</div>
                <div class="panel-body">
                    <!-- <form class="form-horizontal" role="form" method="POST" action="{{ url('register') }}"> -->
                    <!-- <form method="post" class="form-horizontal"  action="{{ url('user') }}"  accept-charset="UTF-8"> -->
                   @if ($errors->has())
			        <div class="alert alert-danger">
			            @foreach ($errors->all() as $error)
			                {{ $error }}<br>        
			            @endforeach
			        </div>
			        @endif
                    {{ Form::open( [ 'url' => 'user', 'class'=>'form-horizontal','method' => 'post', 'files' => true ] ) }}
                        
                       @foreach($user_details as $index=>$user)
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label class="col-md-1 control-label">Name</label>

                            <div class="col-md-4">
                                <!-- <input type="text" class="form-control" name="name" value="{{ old('name') }}"> -->
                                <input type="text" class="form-control" name="name" value="{{$user->userName}}">
                            </div>

                            <label class="col-md-2 control-label">Upload Pic</label>

                            <div class="col-md-4" >
                                
                                <div style="position:relative;">
                                        <a class='btn btn-primary' href='javascript:;'>
                                            Choose File...
                                            <input type="file" style='position:absolute;z-index:2;top:0;left:0;filter: alpha(opacity=0);-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";opacity:0;background-color:transparent;color:transparent;' name="file_source"  size="40"  onchange='$("#upload-file-info").html($(this).val());'>
                                        </a>
                                        &nbsp;
                                        <span class='label label-info' id="upload-file-info"></span>
                                    <div class="img-style-2" style="width:75px;height:75px;margin-left:125px;">	
								<?php if($user->photo == '') {  ?>
									{!! Html::image('public/img/profile/profile.jpg') !!}

								<?php }else{ ?>
									{!! Html::image('public/img/profile/'.$user->photo) !!}
								<?php }
									?>

								</div>

                                </div>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label class="col-md-1 control-label">E-Mail </label>

                            <div class="col-md-4">
                                <input type="email" class="form-control" name="email" value="{{$user->email}}">
                            </div>

                            <label class="col-md-2 control-label">Business Info </label>

                            <div class="col-md-4">
                                <input type="text" class="form-control" name="business" value="<?php if(isset($_SESSION['business'])){echo $_SESSION['business'];} else{echo $user->businessinfo;} ?>" placeholder="http://www.demo.com">
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label class="col-md-1 control-label">Known Languages</label>

                            <div class="col-md-4">
                                <input type="text" class="form-control" name="languages" value="<?php if(isset($_SESSION['laguages'])){echo $_SESSION['laguages'];}else{echo $user->languageknown; } ?>">
                            </div>

                            <label class="col-md-2 control-label">Contact</label>

                            <div class="col-md-4">
                                <input type="text" class="form-control" name="contact" value="<?php if(isset($_SESSION['contact'])){echo $_SESSION['contact'];}else{echo $user->contactinfo; } ?>">

                                
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <label class="col-md-1 control-label">About Me</label>
                            <div class="col-md-6">
                               <!-- <textarea rows="5"  name="about" class="form-control"><?php //if(isset($_SESSION['about'])){echo $_SESSION['about'];}else{echo $user->aboutme;} ?>
								<!--</textarea> -->
								<?php 
								function about($aboutme)
								{
									if(isset($_SESSION['about']))
									{
										return $about=$_SESSION['about'];
									}else
									{
										return $aboutme;	
									}
									
								}

								?>
								{!! Form::textarea('about',about($user->aboutme), ['class'=>'form-control', 'rows'=>5] ) !!}
                            </div>

                            <!--  <button type="submit" class="btn btn-danger">
                                    <i class="fa fa-btn fa-edit"></i>Edit Your Profile
                                </button> -->
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary" data-toggle="modal">
                                    <i class="fa fa-btn fa-user-plus"></i>Update Profile
                                </button>

                                 <a class="btn btn-primary fa fa-reply" href="{{ url('/products') }}">&nbsp;&nbsp;Cancel </a>
                            </div>
                            @endforeach
                        </div>
                        {!! Form::close() !!}
                    <!-- </form> -->
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
<?php 	}
?>
@endsection