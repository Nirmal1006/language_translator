@extends('layouts.app')

@section('content')
<style>
	.img-style img{
		width: 100px;
		height:120px;
		margin-left: 50px;
		
	}
</style>
<div class="container">
<div class="row">

			@foreach($users as $index=>$user)


<div class="container">
		<div class="row">

<div class="col-md-1">			
<?php

	$icon= "fa fa-pencil";
	$edit = "Edit";
	$url="testing";
	$style="";
	$id = Auth::id();
			 if(Auth::check()  && $id == $user->user_id )
{ ?>
		{{ Form::open( [ 'url' => $url, 'class'=>'form-horizontal','method' => 'post', 'files' => true ] ) }}		
				<button class="btn btn-primary" style="<?php echo $style; ?>"><i class="<?php echo $icon; ?>">&nbsp;&nbsp;<?php echo $edit; ?></i></button>	
				<input type="hidden" name="edit" id="edit" value="edit">						
		 {!! Form::close() !!}
			</div>
<?php }else{ }?>
<div class="col-md-1 pull-left">
	

	@if(Session::has('my_search'))
		{{Session::forget('my_search') }}
    <a href="forumsearch" class="btn btn-primary" ><i class="fa fa-reply">&nbsp;&nbsp;Back</i></a>
	@else
	<a href="{{ URL::previous() }}" class="btn btn-primary" ><i class="fa fa-reply">&nbsp;&nbsp;Back</i></a>
	@endif

</div>
<div class="col-md-10"></div>
		</div>
	</div>


				<div class="col-md-6">
					<div class="my-style" style="padding:20px;">
					  	<div class="panel panel-warning">
										  <div class="panel-heading">
										    <h3 class="panel-title">Personal Information:</h3>
										  </div>
						</div>
						<table padding=2px  width="400px" style="margin-left:30px;" > 
							<tr>
						
							<td >Name : {{ $user->userName }} </td>
							<td rowspan="3">
								<div class="img-style" style="width:100px;height:100px;margin-left:125px;" >	
								<?php if($user->photo == '') {  ?>
									{!! Html::image('public/img/profile/profile.jpg') !!}

								<?php }else{ ?>
									{!! Html::image('public/img/profile/'.$user->photo) !!}
								<?php }
									?>
								@if(Auth::check() && $_SESSION['userid'] === $user->id)
								{{ Form::open( [ 'url' => 'remove_img', 'class'=>'form-horizontal','method' => 'post', 'files' => true ] ) }}
								<button class="btn btn-primary btn-xs pull-right"  >Remove Image</button>
								<input type="hidden" name="userid" id="userid" value="<?php $_SESSION['userid']; ?>">
								{!! Form::close() !!}
								@else
								@endif
								</div>
							</td>
						</tr>

						<tr>
							<td>Email : {{ $user->email }} </td>
						</tr>

						<tr>
							<td>Languages Known : {{ $user->languageknown }}</td>
						</tr>
						</table>
						<br> 

						<div class="panel panel-warning">
										  <div class="panel-heading">
										    <h3 class="panel-title">About Me:</h3>
										  </div>
						</div>
						<p>
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ $user->aboutme }}
							<?php $time=$user->created_at;
							?>
						</p>
					</div>
				</div>
				<div class="col-md-6">	
					<div class="table-style" style="padding:15px;"> 
						<div class=" panel-warning">
										  <div class="panel-heading">
										    <h3 class="panel-title">Business Details:</h3>
										  </div>
						</div>
						<br>
						<table padding=2px  width="400px" style="margin-left:30px">
							<tr> 
								<td>Business Information: {{ $user->businessinfo }}</td>
							</tr>
							<tr><td><br></td></tr>
							<tr> 
								<td>Contact Information:{{ $user->contactinfo }}</td>
							</tr>
							<tr><td><br></td></tr>
						</table>
						<div class="panel-warning">
										  <div class="panel-heading">
										    <h3 class="panel-title">Contributions:</h3>
										  </div>
						<div class="panel-body">
									    <div class="list-group">
				  <button type="button" class="list-group-item">Form Post's : <?php echo $forum_count; ?></button>
				  <button type="button" class="list-group-item">Form Comment's : <?php echo $forum_cmt_count; ?></button>
				  <button type="button" class="list-group-item">Glossary(German): <?php echo $my_count['d']; ?></button>
				  <button type="button" class="list-group-item">Glossary(French): <?php echo $my_count['f']; ?></button>
				  <button type="button" class="list-group-item">Glossary(Italian): <?php echo $my_count['i']; ?></button>
				  <button type="button" class="list-group-item">Glossary(Spanish): <?php echo $my_count['s']; ?></button>
				  <button type="button" class="list-group-item">Active From :<?php echo date('M / j / Y g:i A', strtotime($time)); ?>  </button>
				  <button type="button" class="list-group-item">Last Activity: <?php echo date('M / j / Y g:i A', strtotime($user->updated_at)); ?></button>
				</div>
						</div>
					</div> 
				</div>		
			</div>
		</div>
		@endforeach
@endsection