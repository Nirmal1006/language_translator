@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row">
  <div class="panel panel-info">
          <div class="panel-heading">
              <h3 class="panel-title">Forum : <?php switch ($_SESSION['key']) {
            case 0:
                
                echo "German"; 
                break;
            case 1:
                
                echo "Frence"; 
                break;
            case 2:
               
                echo "Italian"; 
                break;
            case 3:
                
                echo "Spanish";
                break;
            default:
               
                echo "German- Default"; 
                break;
        } ?> </h3>
          </div>
  </div><!---Header Over-->

  <hr>
  @if ($errors->has())
  <div class="alert alert-danger">
      @foreach ($errors->all() as $error)
          {{ $error }}<br>        
      @endforeach
  </div>
  @endif
  <div class="col-md-12">
  <div class="col-md-offset-4">
  {{ Form::open( [ 'url' => 'dosearch', 'class'=>'form-inline','method' => 'post', 'files' => true ] ) }}
    <!-- <form class="form-inline" role="form"> -->
    <div class="form-group">
     {!! Form::text('q', isset($q) ? $q : null, ['class'=>'form-control', 'placeholder' => 'Enter the word to search']) !!}
                    {!! $errors->first('q', '<p class="help-block">:message</p>') !!}
     
    </div>
    <button type="submit" class="btn btn-search"><i class="fa fa-search"></i>&nbsp;&nbsp;Search</button>
<a href="{{ url('/newpost') }}" class="btn  btn-primary"><i class="fa  fa-comments"></i>&nbsp;&nbsp;Add New Post</a>

  <!-- </form> -->
  {!! Form::close() !!}
  
  </div>
  </div>
     &nbsp;
     <br>
  <div class="col-md-2"></div>
  <div class="col-md-8">
  
      <div class="table-responsive">          
        <table class="table table-bordered ">
         <?php $seg=readsegment(); $sno=$seg*4+1; ?>
        
        @foreach ($forum as $key=>$forums)
        <?php $time=$forums->updated_at;
          if($forums->user_id == 0){
               $user="Demo User";
            }else{
              $user=username($forums->user_id); }?>
    <!-- <thead> -->
      <tr>
        
        <th>
        <?php if($forums->user_id === 0){
       ?>
        <p>User Name:&nbsp;&nbsp;{{$forums->name}}</p>
        <?php } else{ ?>
          <p>User Name:<a href="/userprofilesearch/?id={{$forums->user_id}}"><?php echo $user['username']; ?></a></p>
        <?php } ?>
        <p>Posted On:&nbsp;&nbsp;<?php echo date('M / j / Y g:i A', strtotime($time)); ?></p>
        </th>
        <th><p class="pull-right">Post No:&nbsp;&nbsp;{{$sno}}</p></th>
 
      </tr>
 <!--  -->
 <tbody>
    <tr>
      <td colspan="2">
        <span style="font-size:22px;">Description:</span><p class="text-justify" style="font-size:18px;padding-left:2em;padding-right:2em;">{{$forums->description}}</p>
        <hr>
      </td>
      <td>
        
<span class="pull-right">
<?php

$post_user_id=get_post_userid($forums->id);

if(isset($_SESSION['userid']))
  {$user_id = $_SESSION['userid'];}else{$user_id=0;}
 if(Auth::check() && isset($_SESSION['username'])) { ?>
    <?php if($post_user_id == $user_id ) { ?>
  {!! Form::open(['url' => 'editforumpost', 'method'=>'post', 'class'=>'form-inline']) !!}
   <button type="submit" class="btn btn-primary fa fa-pencil" aria-hidden="true">&nbsp;&nbsp;Edit</button>
   <input type="hidden" name="forum_id" id="forum_id" value="{{$forums->id}}">
    <input type="hidden" name="user_id" id="user_id" value="<?php echo $user_id; ?>">
    <?php }else {}?>
  {!! Form::close() !!} 
<?php }else{ ?>
  
  <?php } ?>
  </span>
      </td>
    </tr>
    <tr>
    <td colspan="3" style="background-color:#f5f5f5;"><span style="font-size:22px;"></span><p style="font-size:16px;padding-left:2em;padding-right:2em;"></p>
      <!-- My Single Comment -->
<?php 
  $single_comment =get_single_comment($forums->id); 
  foreach ($single_comment as $key => $value) { ?>
  <?php if($value->name == "") {} else{ ?>
    <?php if($value->user_id === 0){ ?>
     <h5>Commented By:&nbsp;&nbsp; {{$value->name}} 
     </p> <p>Comment On:</i>&nbsp;&nbsp;<?php echo date('M / j / Y g:i A', strtotime($value->created_at)); ?></h5>
     <?php } else{ ?>
      <h5>Commented By:&nbsp;&nbsp;<a href="/userprofilesearch/?id={{$value->user_id}}">{{$value->name}}</a>
 
<!---Edit Opens -->
     <div class="edit pull-right" style="margin-right:20px;">
     <!--Call Model -->
      <?php
      //echo $value->id;
      //echo $_SESSION['userid'];
$comment_user_id=get_comment_userid($value->id);
     //echo $comment_user_id;
 if(Auth::check() && isset($_SESSION['username']) && $_SESSION['userid'] == $comment_user_id) {

if (isset($_SESSION['userid'])) {
  # code...
     if($_SESSION['userid'] === $comment_user_id)
     { ?>
{!! Form::open(['url' => 'editforumcomment', 'method'=>'post', 'class'=>'form-inline']) !!}
<button type="submit" class="btn btn-primary fa fa-pencil" aria-hidden="true">&nbsp;&nbsp;Edit Comment</button>
   <input type="hidden" name="comment_id" id="comment_id" value="{{$value->id}}">
    <input type="hidden" name="forum_id" id="forum_id" value="{{$forums->id}}">
{!! Form::close() !!} 
    <?php }else {} }else {} } else{} ?>
     <!--Call Model -->
     </div>
     <!---Edit Close  -->
      </h5>
      <h5>Comment On</i>&nbsp;&nbsp;<?php echo date('M / j / Y g:i A', strtotime($value->created_at)); ?></h5>
      <?php } ?>
    <h4 style="padding-left:2em;padding-right:2em;" class="text-justify">{{$value->comment}}</h4>
    
  <!-- My Single Comment Close -->

    <hr> 
<?php 
  $id=$value->id;
  } } ?>
<!-- Comment Text Post Here -->


  <!-- Comment From Here -->
    </td>
  </tr>

      <tr>
    
        <td colspan="4">
          <!-- Reply Comment Post  -->

<span class="pull-right"> 
 <button data-toggle="collapse" data-target="#<?php echo $forums->id; ?>" class="btn btn-primary fa fa-reply" aria-hidden="true">&nbsp;&nbsp; Reply</button>
</span>
<!-- Reply Comment -->
<div id="<?php echo $forums->id; ?>" class="collapse">
{!! Form::open(['url' => 'fpostcomment', 'method'=>'post', 'class'=>'form-inline']) !!}
   <!--  <form class="form-inline" role="form"> -->

    <div class="form-group">
      <?php if(Auth::check() && isset($_SESSION['username'])) { ?>
      <input type="text" class="form-control" id="text" name= "name" value="<?php echo $_SESSION['username']; ?>" readonly>
      <?php }else{ ?>
       <input type="text" class="form-control" id="text" name= "name" placeholder="Name" required>
       
        <?php }
if(Auth::check() && isset($_SESSION['userid']) )
  {$user_id = $_SESSION['userid'];}else{$user_id=0;}
     ?>   
    <input type="hidden" name="forum_id" id="forum_id" value="{{$forums->id}}">
     <input type="hidden" name="user_id" id="user_id" value="<?php echo $user_id; ?>">
    </div>
    <div class="form-group">
    <?php if(Auth::check() && isset($_SESSION['username'])) { ?>
        <input type="email" name="email" id="email" class="form-control form-control-lg" value="<?php echo $_SESSION['useremail']; ?> " readonly>
      <?php  }else { ?> 
        <input type="email" name="email" id="email" class="form-control form-control-lg" placeholder="Email" >
      <?php } ?> 
     <!--  <input type="password" class="form-control" id="pwd" placeholder="Enter password"> -->
    </div>
    <br><br>
    <!-- <p>&nbsp;&nbsp;</p> -->
        <div class="form-group">
      <textarea name="comment" required  name="editor1" class="form-control" rows="5" cols="50" placeholer="Message" ></textarea>
    </div>
    <br>
    <br>
    <!-- <p>&nbsp;&nbsp;</p> -->
    <button type="submit" class="btn btn-primary"><span class="fa fa-send">&nbsp;&nbsp;Post Comment</span></button>
    <a href="/forumsearch" class="btn btn-primary btn-sm" >
                                <i class="fa fa-reply"></i>&nbsp;&nbsp;Cancel
                                </a>
 <!--  </form> -->
<!-- <a class="pull-right" href="" class="fa fa-comment" aria-hidden="true">&nbsp;&nbsp;Post Comment</a> -->

  {!! Form::close() !!} 
</div>
  </td>
 
      </tr>
    
      <?php $sno++; ?>
 <!--  -->
         @endforeach
        </table>
      </div>
  </div>
  <div class="col-md-2"></div>

 @endsection


 <?php 
  function check_forum($lan_key){
    $lan_forum= \DB::table('forums')
                ->where('language_id', '=', $lan_key)
                ->get();
    return $lan_forum;
  }
  function username($id)
  {
    $single_comment= \DB::table('user_profile')
                ->where('user_id', '=', $id)
                ->get();
    foreach ($single_comment as $key => $value) {
      # code...
       $user= $value->userName;
       $id=$value->id;
    }
    $userdata = array('username' =>$user ,'id' =>$id );
    //print_r($userdata);
    return $userdata;   
  }
function readsegment()
    {         
    if(isset($_GET['page']))
    {
        $current_page=$_GET['page'];
    }else
    {
        $current_page=1;
    }
    $page=$current_page-1;
        return $page;
}
function get_single_comment($id){
  $single_comment= \DB::table('forum_comments')
                ->where('forum_id', '=', $id)
                ->get();
    return $single_comment;            
}
function get_comment($id){
$forum_comments= \DB::table('forum_comments')
                ->where('forum_id', '=', $id)
                ->get();
 return $forum_comments;
}
function get_post_userid($id){
  
  $forums= \DB::table('forums')
                ->where('id', '=', $id)
                ->get();
 foreach ($forums as $key => $value) {
   # code...
    $post_user_id=$value->user_id;
 }
 //echo $post_user_id; 
 return $post_user_id;
}
function get_comment_userid($id){
  $forums= \DB::table('forum_comments')
                ->where('id', '=', $id)
                ->get();
  foreach ($forums as $key => $value) {
    # code...
    $comment_user_id = $value->user_id;
  }
  return $comment_user_id;
}
function get_forum($id){
  $forum= \DB::table('forums')
                ->where('id', '=', $id)
                ->get();
  return $forum;
}
?>
