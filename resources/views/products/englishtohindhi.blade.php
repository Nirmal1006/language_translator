@extends('layouts.app')

@section('content')
<?php 
function readsegment()
    {         
    if(isset($_GET['page']))
    {
        $current_page=$_GET['page'];
    }else
    {
        $current_page=1;
    }
    $page=$current_page-1;
        return $page;
}
$key=$_SESSION['key'];
?>
<div class="container">
    <div class="row">
     <div class="panel panel-info">
          <div class="panel-heading">
              <h3 class="panel-title">Page : <?php switch ($_SESSION['key']) {
            case 0:
                
                echo "Tamil"; 
                break;
            case 1:
                
                echo "Telugu"; 
                break;
            case 2:
               
                echo "Malayalam"; 
                break;
            case 3:
                
                echo "Kanada";
                break;
            case 4:
                
                echo "Hindi";
                break;
            case 5:
                
                echo "Sanskrit";
                break;
            case 6:
                
                echo "Punjabi";
                break;
            case 7:
                
                echo "Marathi";
                break;
            case 8:
                
                echo "Bengali";
                break;

            default:
               
                echo "Tamil- Default"; 
                break;
        } ?> </h3>
          </div>
  </div><!---Header Over-->
        <div class="col-md-3"></div>
        <div class="col-md-6 text-center">
            {!! Form::open(['url' => 'showuser','method'=>'get', 'class'=>'form-inline col-lg']) !!}
            
            <div class="form-group {!! $errors->has('q') ? 'has-error' : '' !!}">
            <input type="hidden" name="key" value="<?php echo $key; ?>"></input>
                {!! Form::text('q', isset($q) ? $q : null, ['class'=>'form-control','id' => 'transliterateTextareat', 'placeholder' => 'Enter The Word to Search']) !!}
                {!! $errors->first('q', '<p class="help-block">:message</p>') !!}
<button type="submit" class="btn btn-primary fa fa-search">&nbsp;&nbsp;Search</button>
            </div>
            <form>
              
              <!-- <a class="btn btn-primary fa fa-refresh" href="{{ url('/') }}">&nbsp;&nbsp;Reload </a> -->
          </form>
          {!! Form::close() !!}    
          <hr>
          <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8">
            <?php $seg=readsegment(); $sno=$seg*15+1; ?>
                <table class="table table-hover table-bordered table-responsive">
                    <thead>
                        <tr>
                            <th style="width: 20px;">S.No</th>
                            <th >English</th>
                            <th >French</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if (count($products) >= 1)
                        @foreach($products as $index=>$product)
                        <tr>
                           <td style=" text-align: center;">{{ $sno}}</td>
                            <td><a href="productdetails/?id={{ $product->user_id }}&rid={{$product->id}}"><?php echo ucwords($product->name); ?></a></td>
                                        <td><a href="productdetails/?id={{ $product->user_id }}&rid={{$product->id}}">
                                        <?php echo ucwords($product->model); ?></a></td>
                        </tr>
                        <?php $sno++; ?>
                        @endforeach
                        @elseif (count($products) <= 1)
                        <tr>
                            <td colspan="3">
                                <h3 class="text-center" style="color: #337AB7;">Sorry!, Search term not found.</h3></td>
                            </tr>
                            @endif                                           
                        </tbody>
                    </table>
                    {!! $products->links() !!}
                </div>
            </div>
            <div class="col-md-2"></div>
        </div>       
    </div>
</div>
    <style type="text/css">
        th{
            width: 200px;
            text-align: center;
        }

    </style>
@endsection


<script type="text/javascript" src="https://www.google.com/jsapi">
    </script>
<script type="text/javascript">
 
      // Load the Google Transliterate API
      google.load("elements", "1", {
            packages: "transliteration"
          });
 
      function onLoad() {
        var options = {
            sourceLanguage:
                google.elements.transliteration.LanguageCode.ENGLISH,
            destinationLanguage:
                [google.elements.transliteration.LanguageCode.HINDI],
            shortcutKey: 'ctrl+g',
            // destinationLanguage:
            //     [google.elements.transliteration.LanguageCode.KANNADA],
            // shortcutKey: 'ctrl+k',
            transliterationEnabled: false
        };
 
        // Create an instance on TransliterationControl with the required
        // options.
        var control =
            new google.elements.transliteration.TransliterationControl(options);
 
        // Enable transliteration in the textbox with id
        // 'transliterateTextarea'.
        control.makeTransliteratable(['transliterateTextareat']);
      }
      google.setOnLoadCallback(onLoad);
    </script>

