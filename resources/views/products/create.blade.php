@extends('layouts.app')

@section('content')
<?php 
if($_SESSION['key'] == 0 )
{
  $id = "transliterateTextarea";
  $id2 = "transliterateTextarea2";
  $mylan= "Tamil";
}elseif($_SESSION['key'] == 1)
{
  $id = "transliterateTextareat";$id2 = "transliterateTextareat2";
  $mylan="Telugu";
}elseif($_SESSION['key'] == 2)
{
  $id = "transliterateTextaream"; $id2 = "transliterateTextaream2";
  $mylan="Malayalam";
}elseif($_SESSION['key'] == 3)
{
  $id = "transliterateTextareak";$id2 = "transliterateTextareak2";
  $mylan="Kannada";
}elseif($_SESSION['key'] == 4)
{
  $id = "transliterateTextareah";$id2 = "transliterateTextareah2";
  $mylan="Hindi";
}elseif($_SESSION['key'] == 5)
{
  $id = "transliterateTextareasa";$id2 = "transliterateTextareasa2";
  $mylan="Sanskrit";
}elseif($_SESSION['key'] == 6)
{
  $id = "transliterateTextareapa";$id2 = "transliterateTextareapa2";
  $mylan="Punjabi";
}elseif($_SESSION['key'] == 7)
{
  $id = "transliterateTextareama";$id2 = "transliterateTextareama2";
  $mylan="Marathi";
}elseif($_SESSION['key'] == 8)
{
  $id = "transliterateTextareabe";$id2 = "transliterateTextareabe2";
  $mylan="Bengali";
}else{}
?>
<div class="container">
    <div class="panel panel-primary">
        <div class="panel-heading"><h3>New Product</h3></div>
        <div class="panel-body">
            <div class="col-md-2"></div>
            <div class="col-md-8">
                {!! Form::open(['route' => 'products.store', 'files' => true])!!}
                <div class="form-group {!! $errors->has('name') ? 'has-error' : '' !!}">
                  {!! Form::label('name', 'Name') !!}
                  {!! Form::text('name', null, ['class'=>'form-control','id'=>$id]) !!}
                  {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
              </div>

              <div class="form-group {!! $errors->has('model') ? 'has-error' : '' !!}">
                  {!! Form::label('model', $mylan) !!}
                  {!! Form::text('model', null, ['class'=>'form-control','id'=>$id2]) !!}
                  {!! $errors->first('model', '<p class="help-block">:message</p>') !!}
              </div>
              <div class="form-group {!! $errors->has('model') ? 'has-error' : '' !!}">
                  <input type="hidden" name="user_id" value="<?php echo $_SESSION['userid']; ?>">
                </div>
              

              <div class="form-group {!! $errors->has('language') ? 'has-error' : '' !!}">
                  <input name="language" type="hidden" value="<?php echo $key; ?>">
                  {!! $errors->first('model', '<p class="help-block">:message</p>') !!}
              </div>
              {!! Form::submit(isset($model) ? 'Update' : 'Save', ['class'=>'btn btn-primary']) !!}
              {!! Form::close() !!}</div>
              <div class="col-md-2"></div>
          </div> 
          <div class="panel-footer"></div>
      </div>
  </div>
  
  
  @endsection
              <!-- Tamil Translate Script -->
 <script type="text/javascript" src="https://www.google.com/jsapi">
    </script>
    <script type="text/javascript">
 
      // Load the Google Transliterate API
      google.load("elements", "1", {
            packages: "transliteration"
          });
 
      function onLoad() {
        var options = {
            sourceLanguage:
                google.elements.transliteration.LanguageCode.ENGLISH,
            destinationLanguage:
                [google.elements.transliteration.LanguageCode.TAMIL],
            shortcutKey: 'ctrl+g',
            // destinationLanguage:
            //     [google.elements.transliteration.LanguageCode.KANNADA],
            // shortcutKey: 'ctrl+k',
            transliterationEnabled: false
        };
 
        // Create an instance on TransliterationControl with the required
        // options.
        var control =
            new google.elements.transliteration.TransliterationControl(options);
 
        // Enable transliteration in the textbox with id
        // 'transliterateTextarea'.
        control.makeTransliteratable(['transliterateTextarea']);
      }
      google.setOnLoadCallback(onLoad);
    </script>

    <script type="text/javascript">
 
      // Load the Google Transliterate API
      google.load("elements", "1", {
            packages: "transliteration"
          });
 
      function onLoad() {
        var options = {
            sourceLanguage:
                google.elements.transliteration.LanguageCode.ENGLISH,
            destinationLanguage:
                [google.elements.transliteration.LanguageCode.KANNADA],
            shortcutKey: 'ctrl+g',
            // destinationLanguage:
            //     [google.elements.transliteration.LanguageCode.KANNADA],
            // shortcutKey: 'ctrl+k',
            transliterationEnabled: false
        };
 
        // Create an instance on TransliterationControl with the required
        // options.
        var control =
            new google.elements.transliteration.TransliterationControl(options);
 
        // Enable transliteration in the textbox with id
        // 'transliterateTextarea'.
        control.makeTransliteratable(['transliterateTextareak']);
      }
      google.setOnLoadCallback(onLoad);
    </script>

<script type="text/javascript">
 
      // Load the Google Transliterate API
      google.load("elements", "1", {
            packages: "transliteration"
          });
 
      function onLoad() {
        var options = {
            sourceLanguage:
                google.elements.transliteration.LanguageCode.ENGLISH,
            destinationLanguage:
                [google.elements.transliteration.LanguageCode.TELUGU],
            shortcutKey: 'ctrl+g',
            // destinationLanguage:
            //     [google.elements.transliteration.LanguageCode.KANNADA],
            // shortcutKey: 'ctrl+k',
            transliterationEnabled: false
        };
 
        // Create an instance on TransliterationControl with the required
        // options.
        var control =
            new google.elements.transliteration.TransliterationControl(options);
 
        // Enable transliteration in the textbox with id
        // 'transliterateTextarea'.
        control.makeTransliteratable(['transliterateTextareat']);
      }
      google.setOnLoadCallback(onLoad);
    </script>


<script type="text/javascript">
 
      // Load the Google Transliterate API
      google.load("elements", "1", {
            packages: "transliteration"
          });
 
      function onLoad() {
        var options = {
            sourceLanguage:
                google.elements.transliteration.LanguageCode.ENGLISH,
            destinationLanguage:
                [google.elements.transliteration.LanguageCode.MALAYALAM],
            shortcutKey: 'ctrl+g',
            // destinationLanguage:
            //     [google.elements.transliteration.LanguageCode.KANNADA],
            // shortcutKey: 'ctrl+k',
            transliterationEnabled: false
        };
 
        // Create an instance on TransliterationControl with the required
        // options.
        var control =
            new google.elements.transliteration.TransliterationControl(options);
 
        // Enable transliteration in the textbox with id
        // 'transliterateTextarea'.
        control.makeTransliteratable(['transliterateTextaream']);
      }
      google.setOnLoadCallback(onLoad);
    </script>
     <script type="text/javascript">
 
      // Load the Google Transliterate API
      google.load("elements", "1", {
            packages: "transliteration"
          });
 
      function onLoad() {
        var options = {
            sourceLanguage:
                google.elements.transliteration.LanguageCode.ENGLISH,
            destinationLanguage:
                [google.elements.transliteration.LanguageCode.HINDI],
            shortcutKey: 'ctrl+g',
            // destinationLanguage:
            //     [google.elements.transliteration.LanguageCode.KANNADA],
            // shortcutKey: 'ctrl+k',
            transliterationEnabled: false
        };
 
        // Create an instance on TransliterationControl with the required
        // options.
        var control =
            new google.elements.transliteration.TransliterationControl(options);
 
        // Enable transliteration in the textbox with id
        // 'transliterateTextarea'.
        control.makeTransliteratable(['transliterateTextareah']);
      }
      google.setOnLoadCallback(onLoad);
    </script>
      <script type="text/javascript">
 
      // Load the Google Transliterate API
      google.load("elements", "1", {
            packages: "transliteration"
          });
 
      function onLoad() {
        var options = {
            sourceLanguage:
                google.elements.transliteration.LanguageCode.ENGLISH,
            destinationLanguage:
                [google.elements.transliteration.LanguageCode.SANSKRIT],
            shortcutKey: 'ctrl+g',
            // destinationLanguage:
            //     [google.elements.transliteration.LanguageCode.KANNADA],
            // shortcutKey: 'ctrl+k',
            transliterationEnabled: false
        };
 
        // Create an instance on TransliterationControl with the required
        // options.
        var control =
            new google.elements.transliteration.TransliterationControl(options);
 
        // Enable transliteration in the textbox with id
        // 'transliterateTextarea'.
        control.makeTransliteratable(['transliterateTextareasa']);
      }
      google.setOnLoadCallback(onLoad);
    </script>
      <script type="text/javascript">
 
      // Load the Google Transliterate API
      google.load("elements", "1", {
            packages: "transliteration"
          });
 
      function onLoad() {
        var options = {
            sourceLanguage:
                google.elements.transliteration.LanguageCode.ENGLISH,
            destinationLanguage:
                [google.elements.transliteration.LanguageCode.PUNJABI],
            shortcutKey: 'ctrl+g',
            // destinationLanguage:
            //     [google.elements.transliteration.LanguageCode.KANNADA],
            // shortcutKey: 'ctrl+k',
            transliterationEnabled: false
        };
 
        // Create an instance on TransliterationControl with the required
        // options.
        var control =
            new google.elements.transliteration.TransliterationControl(options);
 
        // Enable transliteration in the textbox with id
        // 'transliterateTextarea'.
        control.makeTransliteratable(['transliterateTextareapa']);
      }
      google.setOnLoadCallback(onLoad);
    </script>
  <script type="text/javascript">
 
      // Load the Google Transliterate API
      google.load("elements", "1", {
            packages: "transliteration"
          });
 
      function onLoad() {
        var options = {
            sourceLanguage:
                google.elements.transliteration.LanguageCode.ENGLISH,
            destinationLanguage:
                [google.elements.transliteration.LanguageCode.MARATHI],
            shortcutKey: 'ctrl+g',
            // destinationLanguage:
            //     [google.elements.transliteration.LanguageCode.KANNADA],
            // shortcutKey: 'ctrl+k',
            transliterationEnabled: false
        };
 
        // Create an instance on TransliterationControl with the required
        // options.
        var control =
            new google.elements.transliteration.TransliterationControl(options);
 
        // Enable transliteration in the textbox with id
        // 'transliterateTextarea'.
        control.makeTransliteratable(['transliterateTextareama']);
      }
      google.setOnLoadCallback(onLoad);
    </script>
  <script type="text/javascript">
 
      // Load the Google Transliterate API
      google.load("elements", "1", {
            packages: "transliteration"
          });
 
      function onLoad() {
        var options = {
            sourceLanguage:
                google.elements.transliteration.LanguageCode.ENGLISH,
            destinationLanguage:
                [google.elements.transliteration.LanguageCode.BENGALI],
            shortcutKey: 'ctrl+g',
            // destinationLanguage:
            //     [google.elements.transliteration.LanguageCode.KANNADA],
            // shortcutKey: 'ctrl+k',
            transliterationEnabled: false
        };
 
        // Create an instance on TransliterationControl with the required
        // options.
        var control =
            new google.elements.transliteration.TransliterationControl(options);
 
        // Enable transliteration in the textbox with id
        // 'transliterateTextarea'.
        control.makeTransliteratable(['transliterateTextareabe']);
      }
      google.setOnLoadCallback(onLoad);
    </script>






    <script type="text/javascript" src="https://www.google.com/jsapi">
    </script>
    <script type="text/javascript">
 
      // Load the Google Transliterate API
      google.load("elements", "1", {
            packages: "transliteration"
          });
 
      function onLoad() {
        var options = {
            sourceLanguage:
                google.elements.transliteration.LanguageCode.ENGLISH,
            destinationLanguage:
                [google.elements.transliteration.LanguageCode.TAMIL],
            shortcutKey: 'ctrl+g',
            // destinationLanguage:
            //     [google.elements.transliteration.LanguageCode.KANNADA],
            // shortcutKey: 'ctrl+k',
            transliterationEnabled: false
        };
 
        // Create an instance on TransliterationControl with the required
        // options.
        var control =
            new google.elements.transliteration.TransliterationControl(options);
 
        // Enable transliteration in the textbox with id
        // 'transliterateTextarea'.
        control.makeTransliteratable(['transliterateTextarea2']);
      }
      google.setOnLoadCallback(onLoad);
    </script>

    <script type="text/javascript">
 
      // Load the Google Transliterate API
      google.load("elements", "1", {
            packages: "transliteration"
          });
 
      function onLoad() {
        var options = {
            sourceLanguage:
                google.elements.transliteration.LanguageCode.ENGLISH,
            destinationLanguage:
                [google.elements.transliteration.LanguageCode.KANNADA],
            shortcutKey: 'ctrl+g',
            // destinationLanguage:
            //     [google.elements.transliteration.LanguageCode.KANNADA],
            // shortcutKey: 'ctrl+k',
            transliterationEnabled: false
        };
 
        // Create an instance on TransliterationControl with the required
        // options.
        var control =
            new google.elements.transliteration.TransliterationControl(options);
 
        // Enable transliteration in the textbox with id
        // 'transliterateTextarea'.
        control.makeTransliteratable(['transliterateTextareak2']);
      }
      google.setOnLoadCallback(onLoad);
    </script>

<script type="text/javascript">
 
      // Load the Google Transliterate API
      google.load("elements", "1", {
            packages: "transliteration"
          });
 
      function onLoad() {
        var options = {
            sourceLanguage:
                google.elements.transliteration.LanguageCode.ENGLISH,
            destinationLanguage:
                [google.elements.transliteration.LanguageCode.TELUGU],
            shortcutKey: 'ctrl+g',
            // destinationLanguage:
            //     [google.elements.transliteration.LanguageCode.KANNADA],
            // shortcutKey: 'ctrl+k',
            transliterationEnabled: false
        };
 
        // Create an instance on TransliterationControl with the required
        // options.
        var control =
            new google.elements.transliteration.TransliterationControl(options);
 
        // Enable transliteration in the textbox with id
        // 'transliterateTextarea'.
        control.makeTransliteratable(['transliterateTextareat2']);
      }
      google.setOnLoadCallback(onLoad);
    </script>


<script type="text/javascript">
 
      // Load the Google Transliterate API
      google.load("elements", "1", {
            packages: "transliteration"
          });
 
      function onLoad() {
        var options = {
            sourceLanguage:
                google.elements.transliteration.LanguageCode.ENGLISH,
            destinationLanguage:
                [google.elements.transliteration.LanguageCode.MALAYALAM],
            shortcutKey: 'ctrl+g',
            // destinationLanguage:
            //     [google.elements.transliteration.LanguageCode.KANNADA],
            // shortcutKey: 'ctrl+k',
            transliterationEnabled: false
        };
 
        // Create an instance on TransliterationControl with the required
        // options.
        var control =
            new google.elements.transliteration.TransliterationControl(options);
 
        // Enable transliteration in the textbox with id
        // 'transliterateTextarea'.
        control.makeTransliteratable(['transliterateTextaream2']);
      }
      google.setOnLoadCallback(onLoad);
    </script>
     <script type="text/javascript">
 
      // Load the Google Transliterate API
      google.load("elements", "1", {
            packages: "transliteration"
          });
 
      function onLoad() {
        var options = {
            sourceLanguage:
                google.elements.transliteration.LanguageCode.ENGLISH,
            destinationLanguage:
                [google.elements.transliteration.LanguageCode.HINDI],
            shortcutKey: 'ctrl+g',
            // destinationLanguage:
            //     [google.elements.transliteration.LanguageCode.KANNADA],
            // shortcutKey: 'ctrl+k',
            transliterationEnabled: false
        };
 
        // Create an instance on TransliterationControl with the required
        // options.
        var control =
            new google.elements.transliteration.TransliterationControl(options);
 
        // Enable transliteration in the textbox with id
        // 'transliterateTextarea'.
        control.makeTransliteratable(['transliterateTextareah2']);
      }
      google.setOnLoadCallback(onLoad);
    </script>
      <script type="text/javascript">
 
      // Load the Google Transliterate API
      google.load("elements", "1", {
            packages: "transliteration"
          });
 
      function onLoad() {
        var options = {
            sourceLanguage:
                google.elements.transliteration.LanguageCode.ENGLISH,
            destinationLanguage:
                [google.elements.transliteration.LanguageCode.SANSKRIT],
            shortcutKey: 'ctrl+g',
            // destinationLanguage:
            //     [google.elements.transliteration.LanguageCode.KANNADA],
            // shortcutKey: 'ctrl+k',
            transliterationEnabled: false
        };
 
        // Create an instance on TransliterationControl with the required
        // options.
        var control =
            new google.elements.transliteration.TransliterationControl(options);
 
        // Enable transliteration in the textbox with id
        // 'transliterateTextarea'.
        control.makeTransliteratable(['transliterateTextareasa2']);
      }
      google.setOnLoadCallback(onLoad);
    </script>
      <script type="text/javascript">
 
      // Load the Google Transliterate API
      google.load("elements", "1", {
            packages: "transliteration"
          });
 
      function onLoad() {
        var options = {
            sourceLanguage:
                google.elements.transliteration.LanguageCode.ENGLISH,
            destinationLanguage:
                [google.elements.transliteration.LanguageCode.PUNJABI],
            shortcutKey: 'ctrl+g',
            // destinationLanguage:
            //     [google.elements.transliteration.LanguageCode.KANNADA],
            // shortcutKey: 'ctrl+k',
            transliterationEnabled: false
        };
 
        // Create an instance on TransliterationControl with the required
        // options.
        var control =
            new google.elements.transliteration.TransliterationControl(options);
 
        // Enable transliteration in the textbox with id
        // 'transliterateTextarea'.
        control.makeTransliteratable(['transliterateTextareapa2']);
      }
      google.setOnLoadCallback(onLoad);
    </script>
  <script type="text/javascript">
 
      // Load the Google Transliterate API
      google.load("elements", "1", {
            packages: "transliteration"
          });
 
      function onLoad() {
        var options = {
            sourceLanguage:
                google.elements.transliteration.LanguageCode.ENGLISH,
            destinationLanguage:
                [google.elements.transliteration.LanguageCode.MARATHI],
            shortcutKey: 'ctrl+g',
            // destinationLanguage:
            //     [google.elements.transliteration.LanguageCode.KANNADA],
            // shortcutKey: 'ctrl+k',
            transliterationEnabled: false
        };
 
        // Create an instance on TransliterationControl with the required
        // options.
        var control =
            new google.elements.transliteration.TransliterationControl(options);
 
        // Enable transliteration in the textbox with id
        // 'transliterateTextarea'.
        control.makeTransliteratable(['transliterateTextareama2']);
      }
      google.setOnLoadCallback(onLoad);
    </script>
  <script type="text/javascript">
 
      // Load the Google Transliterate API
      google.load("elements", "1", {
            packages: "transliteration"
          });
 
      function onLoad() {
        var options = {
            sourceLanguage:
                google.elements.transliteration.LanguageCode.ENGLISH,
            destinationLanguage:
                [google.elements.transliteration.LanguageCode.BENGALI],
            shortcutKey: 'ctrl+g',
            // destinationLanguage:
            //     [google.elements.transliteration.LanguageCode.KANNADA],
            // shortcutKey: 'ctrl+k',
            transliterationEnabled: false
        };
 
        // Create an instance on TransliterationControl with the required
        // options.
        var control =
            new google.elements.transliteration.TransliterationControl(options);
 
        // Enable transliteration in the textbox with id
        // 'transliterateTextarea'.
        control.makeTransliteratable(['transliterateTextareabe2']);
      }
      google.setOnLoadCallback(onLoad);
    </script>