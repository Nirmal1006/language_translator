@extends('layouts.app')

@section('content')
<?php if(isset($_SESSION['userid']) && Auth::check() ){
    $user_id=$_SESSION['userid'];
    //$chk_user_id=check_userprofile($user_id);
    }else{
        $user_id=0;
        } ?>

<div class="container">
	<div class="row">
    <div class="panel panel-primary">
     <div class="panel-heading">
                        <h3 class="panel-title">Forum Post On: <?php switch ($_SESSION['key']) {
            case 0:
                
                echo "German"; 
                break;
            case 1:
                
                echo "Frence"; 
                break;
            case 2:
               
                echo "Italian"; 
                break;
            case 3:
                
                echo "Spanish";
                break;
            default:
               
                echo "German- Default"; 
                break;
        } ?> </h3>
                  </div>
        </div>
		<div class="col-md-12">
			{{ Form::open( [ 'url' => 'addpost', 'class'=>'form-horizontal','method' => 'post', 'files' => true ] ) }}
				<div class="row">
			<label class="col-md-2 control-label">Name</label>
                            
                            <div class="col-md-6">
                            <?php if(Auth::check() ) { ?>
                                <input type="text" class="form-control" name="name" value="<?php echo $_SESSION['username'];  ?> " readonly  required>
                             <?php  }else { ?>  
                                <input type="text" class="form-control" name="name" value="" required>
                             <?php } ?>   
                                <input type="hidden" class="form-control" name="language_id" value="<?php echo $_SESSION['key']; ?>" >
                            </div>
            <div class="col-md-4"></div></div>
            <br>
            <div class="row">
            <label class="col-md-2 control-label">Heading</label>

                            <div class="col-md-6">
                                <input type="text" class="form-control" name="heading" value="" required>
                            </div>
            <div class="col-md-4"></div></div>

            <br>
            <div class="row">
            <label class="col-md-2 control-label">E-mail</label>

                            <div class="col-md-6">
                            <?php if(Auth::check() ) { ?>
                                <input type="email" class="form-control" id="email" for="email" name="email" value="<?php echo $_SESSION['useremail'];  ?> " readonly>
                            <?php  }else { ?>
                                 <input type="email" class="form-control" id="email" for="email" name="email" value=" ">
                            <?php } ?> 
                            </div>
                            <div class="col-md-6">
                                <input type="hidden" class="form-control" id="user_id"  name="user_id" value="<?php echo $user_id; ?>">
                            </div>
            <div class="col-md-4"></div></div>
            <br>
            <div class="row">
            <label class="col-md-2 control-label">Description</label>

                            <div class="col-md-6">
                                <textarea rows="5"  name="description" class="form-control"></textarea> 
                            </div>
            <div class="col-md-4"></div></div>
            <br>
				<div class="form-group">
                            <div class="col-md-10 col-md-offset-2">
                                <button type="submit" class="btn btn-primary" data-toggle="modal">
                                    <i class="fa fa-btn fa-send"></i>Submit
                                </button>
                                <a href="/forumsearch" class="btn btn-primary" data-toggle="modal">
                                <i class="fa fa-btn fa-reply"></i>&nbsp;&nbsp;Cancel
                                </a>
                            </div>
                           
                        </div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
 @endsection
<?php 
// function check_userprofile($user_id)
// {
//     $single_comment= \DB::table('user_profile')
//                 ->where('user_id', '=', $id)
//                 ->get();
// }
?>