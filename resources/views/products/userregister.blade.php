@extends('layouts.app')

@section('content')
<?php //echo $_SESSION['key'].$_SESSION['username'].$_SESSION['userid'];?>
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-primary">
                <div class="panel-heading">Register</div>
                <div class="panel-body">
                    <!-- <form class="form-horizontal" role="form" method="POST" action="{{ url('register') }}"> -->
                    <!-- <form method="post" class="form-horizontal"  action="{{ url('user') }}"  accept-charset="UTF-8"> -->
                   
                    {{ Form::open( [ 'url' => 'user', 'class'=>'form-horizontal','method' => 'post', 'files' => true ] ) }}
                        {!! csrf_field() !!}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label class="col-md-1 control-label">Name</label>

                            <div class="col-md-4">
                                <!-- <input type="text" class="form-control" name="name" value="{{ old('name') }}"> -->
                                <input type="text" class="form-control" name="name" value="<?php echo $_SESSION['username'];?>">
                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <label class="col-md-2 control-label">Upload Pic</label>

                            <div class="col-md-4" >
                                
                                <div style="position:relative;">
                                        <a class='btn btn-primary' href='javascript:;'>
                                            Choose File...
                                            <input type="file" style='position:absolute;z-index:2;top:0;left:0;filter: alpha(opacity=0);-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";opacity:0;background-color:transparent;color:transparent;' name="file_source"  size="40"  onchange='$("#upload-file-info").html($(this).val());'>
                                        </a>
                                        &nbsp;
                                        <span class='label label-info' id="upload-file-info"></span>
                                </div>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label class="col-md-1 control-label">E-Mail </label>

                            <div class="col-md-4">
                                <input type="email" class="form-control" name="email" value="<?php echo $_SESSION['useremail']; ?>">
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <label class="col-md-2 control-label">Business Info </label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="business" value="{{ old('email') }}">
                            </div>
                        </div>
                        
                            <label class="col-md-1 control-label">Known Languages</label>

                            <div class="col-md-4">
                                <input type="text" class="form-control" name="languages">

                            </div>

                            <label class="col-md-2 control-label">Contact</label>

                            <div class="col-md-4">
                                <input type="text" class="form-control" name="contact">

                               
                            </div>
                        
                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <label class="col-md-1 control-label">About Me</label>

                            <div class="col-md-6">
                                {!! Form::textarea('about', null, ['class'=>'form-control', 'rows'=>5] ) !!}
                            </div>

                            <!--  <button type="submit" class="btn btn-danger">
                                    <i class="fa fa-btn fa-edit"></i>Edit Your Profile
                                </button> -->
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-user-plus"></i>Register
                                </button>

                                 <a class="btn btn-primary fa fa-reply" href="{{ url('/userprofile') }}">&nbsp;&nbsp;View Profile </a>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    <!-- </form> -->
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
