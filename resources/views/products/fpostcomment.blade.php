@extends('layouts.app')

@section('content')

<?php 
//echo $f =$_SESSION['forum_id'];
if(isset($forum_comments))
{
	//print_r($forum_comments);

	//print_r($forum);
	// foreach ($forum_comments as $key => $comment) {
	// //echo $comment->name.'<br>';
	// //echo $comment->email.'<br>';
	// //echo $comment->comment.'<br>'; 	
	// }
}else{}
if(isset($_SESSION['userid']) && Auth::check()){
    $user_id=$_SESSION['userid'];
    }else{
        $user_id=0;
        } 

	function username($id)
	{
		$con=mysqli_connect("localhost","schanetz_schanet","schanetz2016","schanetz_translate");
		$sql="SELECT * FROM `user_profile` WHERE `user_id` = $id";
		$result = mysqli_query($con,$sql);

		if ($result->num_rows > 0) {
		  
		    while($row = $result->fetch_assoc()) {
		        //print_r($row);

		    //echo $row->username;
		        $user=$row['userName'];
		        $id = $row['id'];

		    }
		} else {
		    //$user=$id;	
		}
		$userdata = array('username' =>$user ,'id' =>$id );
		return $userdata;
	}
	function comment_username($id)
	{
		$con=mysqli_connect("localhost","schanetz_schanet","schanetz2016","schanetz_translate");
		$sql="SELECT * FROM `user_profile` WHERE `user_id` = $id";
		$result = mysqli_query($con,$sql);

		if ($result->num_rows > 0) {
		  
		    while($row = $result->fetch_assoc()) {
		        //print_r($row);

		    //echo $row->username;
		        $user=$row['userName'];
		        $id = $row['id'];
		    }
		} else {
		    //$user=$id;	
		}
		$userdata = array('username' =>$user ,'id' =>$id );
		return $userdata;
	}
?>

<div class="container">
	<div class="row">
		 <div class="panel-body">

				  @foreach ($forum as $key=>$forum)

				  	<?php 
				  		$time=$forum->created_at;
				  		if($forum->user_id === 0){
				  	   $user="Demo User";
				  	}else{
				  		$user=username($forum->user_id);

				  		} 
				  	 ?>
						<table class="table tabl-resposive table-bordered">
							<tr >
								<td><span style="font-size:22px;">Title :</span><span style="font-size:18px;">{{$forum->heading}}</span></td> 	
	<?php if($forum->user_id === 0){
			 ?>
<td><span style="font-size:22px;">Posted By :</span><span style="font-size:18px;">{{$forum->name}}</span></td>
<?php } else{ ?>	
<td><span style="font-size:22px;">Posted By :</span><span style="font-size:18px;"><a href="/userprofilesearch/?id=<?php echo $user['id']; ?>"><?php echo $user['username']; ?></a></span></td>	
<?php } ?>

								<td><span style="font-size:22px;">Posted On :</span><span style="font-size:18px;"><?php echo date('M / j / Y g:i A', strtotime($time)); ?></span></td>	
							</tr>
							<tr >
								<td colspan="3"><span style="font-size:22px;">Description :</span><p style="font-size:18px;padding-left:5em;">{{  $forum->description}}</p>
								</td>

							</tr>
							<tr>
							 {!! Form::open(['url' => 'fpostcomment', 'method'=>'post', 'class'=>'form-inline col-lg']) !!}
								<td colspan="3">
								<br>
									<div class="comments" >

									<span style="font-size:22px;">Comments :</span>
										@foreach($forum_comments as $key=>$comment)

											@if($key != 0)
											<hr style="border: 1px solid rgb(149, 134, 134); margin-top: 5px; margin-bottom: 5px;">
											@endif
											<?php 
												if($comment->user_id === 0){
											  	   $cuser="Demo User";
											  	}else{

											  		$cuser=comment_username($comment->user_id);

											  		} 
											 ?>
											<div class="container">
												<div class="row">
													<div class="col-md-6">
	<?php if($comment->user_id === 0){ ?>
			<h4 class="text-capitalize">{{$comment->name}}</h4>
	<?php }else { ?>
			<h4 class="text-capitalize"><a href="/userprofilesearch/?id=<?php //echo $user['id']; ?>"><?php //echo $cuser['username']; ?></a></h4>
		<?php } ?> 
													</div>
													<div class="col-md-6">
													
														<p><?php echo date('M / j / Y g:i A', strtotime($comment->created_at)); ?></p>
													</div>
												</div>
												<div class="row">
													<div class="col-md-12">
														<p>
														{{$comment->comment}}	
														</p>
													</div>
												</div>
											</div>
											

										@endforeach
										{!! $forum_comments->appends(array('id' => $_GET['id']))->links() !!}

									</div>
								<br>
								<span style="font-size:22px;">Post Your Comments :</span>

								<div class="form-group">

									<div class="col-md-6">
									<?php if(Auth::check() && isset($_SESSION['username'])) { ?>
										<input type="text" class="form-control" name= "name"  id="name"  value="<?php echo $_SESSION['username']; ?>" readonly>
									<?php  }else { ?> 
										<input type="text" class="form-control" name= "name"  id="name" placeholder="Name" required>
									<?php } ?>   
										<br>
										
										<input type="hidden" name="forum_id" id="forum_id" value="<?php echo $forum->id; ?>">
										<input type="hidden" name="user_id" id="user_id" value="<?php echo $user_id; ?>">
									</div>
									<div class="col-md-6">
									<?php if(Auth::check() && isset($_SESSION['username'])) { ?>
										<input type="email" name="email" id="email" class="form-control" value="<?php echo $_SESSION['useremail']; ?> " readonly>
									<?php  }else { ?> 
										<input type="email" name="email" id="email" class="form-control" placeholder="Eamil">
									<?php } ?> 
										<br>
									</div>
									<br>
									<div class="col-md-12">
									<textarea name="comment" id="comment" class="form-control" cols="30" rows="6" placeholder="Comments" required></textarea>
									<br>	
									</div>
									
									<div class="col-md-12">
									<button class="btn btn-primary pull-right"> <span class="fa fa-send">&nbsp;&nbsp;Post</span></button>	
									</div>
								</div>

								</td>
							{!! Form::close() !!} 
							</tr>
							
						</table>
						<hr>
						@endforeach


				  </div>
	</div>
</div>

@endsection