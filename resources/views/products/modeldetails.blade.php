@extends('layouts.app')

@section('content')

<div class="container">
	<div class="row">
		<div class="col-md-1 pull-left">
	<a href="{{ url()->previous() }}" class="btn btn-primary"><i class="fa fa-reply">&nbsp;&nbsp;Back</i></a>

</div>
<div class="col-md-11"></div>
	</div>
	<div class="row">
		<div class="col-md-2"></div>
		<div class="col-md-8">
			<?php 
			//print_r($user_info);
			
			foreach ($row_info as $key => $model) {
				
				$lan=$model->language;
				$created_at=$model->created_at;
				$updated_at=$model->updated_at;
				$rmodel=$model->model;
				$rname=$model->name;
				$id=$model->id;
			}
			switch ($lan) {
				case '0':
					$l="Deutsch";
					break;
				case '1':
					$l="French";
					break;
					case '2':
					$l="Italian";
					break;
					case '3':
					$l="Spanish";
					break;
				default:
				
					break;
			}
			?>
		<div class="panel panel-primary">
				<div class="panel-heading">
				    <h3 class="panel-title">Model Details</h3>
				 </div>
				 <div class="panel-body">
				 <table class="table table-hover table-bordered table-responsive">
				 	<thead>
				 		<td>English</td>
				 		<td><?php echo $l; ?></td>
				 	<?php if(Auth::check() &&  $_SESSION['userid']===1 ){ ?> 
				 		<th style="text-align:center"><i class="fa fa-cog fa-1x" aria-hidden="true"></i></th>
				 	<?php }else {} ?>
				 		<td>User Name</td>
				 		<td>Email</td>
				 		<td>Created On</td>
				 		<td>Updated On</td>
				 	</thead>
				 	<tbody>
				 		<td><?php echo $rname; ?></td>
				 		<td><?php echo $rmodel; ?></td>
				 		<?php if(Auth::check() &&  $_SESSION['userid']===1 ){ ?> 
				 		<td> {!! Form::model( ['route' => ['products.destroy'], 'method' => 'delete', 'class' => 'form-inline'] ) !!}
                                            <a href="{{ route('products.edit', $id)}}" style="padding-right: 10px;" ><i class="fa fa-pencil" aria-hidden="true"></i>
                                            </a> 
                                            <a href="{{ route('products.destroy', $id)}}" class="js-submit-confirm"><i class="fa fa-trash" aria-hidden="true"></i>
                                            </a> 
                                            {!! Form::close()!!}
                        </td>
                       <?php }else {} ?>
						<td><a href="/userprofilesearch/?id=<?php echo $user_info['user_id']; ?>"><?php  echo $user_info['user_name']; ?></a></td>	
						<td><a href="/userprofilesearch/?id=<?php echo $user_info['user_id']; ?>"><?php  echo $user_info['user_email']; ?></a></td>
						<td><?php echo date('M / j / Y g:i A', strtotime($created_at)); ?></td>		
						<td><?php echo date('M / j / Y g:i A', strtotime($updated_at)); ?></td>				
				 	</tbody>
				 </table>
				  
				 </div>
				
			</div>
		</div>
		<div class="col-md-2"></div>
	</div>
</div>

@endsection

