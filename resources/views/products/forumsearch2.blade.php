@extends('layouts.app')

@section('content')

<?php 
if(isset($forum_search_result))
  foreach ($forum_search_result as $key => $keys) {
    $forum_id=$keys->forum_id;
    $forum= get_forum($forum_id); }else{}?>
<?php 
if(isset($forum_search))
{
  //print_r($forum_search);
  }else {

    } ?>
<div class="container">
  <div class="row">
  <div class="panel panel-info">
          <div class="panel-heading">
              <h3 class="panel-title">Forum : <?php switch ($_SESSION['key']) {
            case 0:
                
                echo "German"; 
                break;
            case 1:
                
                echo "Frence"; 
                break;
            case 2:
               
                echo "Italian"; 
                break;
            case 3:
                
                echo "Spanish";
                break;
            default:
               
                echo "German- Default"; 
                break;
        } ?> </h3>
          </div>
  </div><!---Header Over-->

  <hr>
  @if ($errors->has())
  <div class="alert alert-danger">
      @foreach ($errors->all() as $error)
          {{ $error }}<br>        
      @endforeach
  </div>
  @endif
  <div class="col-md-12">
  <div class="col-md-offset-4">
  {{ Form::open( [ 'url' => 'dosearch', 'class'=>'form-inline','method' => 'post', 'files' => true ] ) }}
    <!-- <form class="form-inline" role="form"> -->
    <div class="form-group">
     {!! Form::text('q', isset($q) ? $q : null, ['class'=>'form-control', 'placeholder' => 'Enter the word to search']) !!}
                    {!! $errors->first('q', '<p class="help-block">:message</p>') !!}
     
    </div>
    <button type="submit" class="btn btn-search"><i class="fa fa-search"></i>&nbsp;&nbsp;Search</button>
<a href="{{ url('/newpost') }}"  class="btn  btn-primary"><i class="fa  fa-comments"></i>&nbsp;&nbsp;Add New Post</a>

  <!-- </form> -->
  {!! Form::close() !!}
  
  </div>
  </div>
     &nbsp;
  
  
  <div class="col-md-2"></div>
  <div class="col-md-8">
  
      <div class="table-responsive">          
        <table class="table table-bordered ">
        <?php $seg=readsegment(); $sno=$seg*4+1; ?>
        @if (count($forum) >= 1)
        <!-- <thead> -->
        <tr>
          <th>Post No</th>
          <th>User Name</th>
          <th>Title </th>
          <th>Tag</th>
          <th>Created At </th>
        </tr>    
    <!-- </thead> -->
        @foreach ($forum as $key=>$forums)
        <?php $time=$forums->updated_at;
          if($forums->user_id == 0){
               $user="Demo User";
            }else{
              $user=username($forums->user_id); }?>
    
    <tbody>
      <tr>
          
        <td>{{$sno}}</td>
        <td>
          <?php if($forums->user_id === 0){
       ?>
        <p>{{$forums->name}}</p>

        <?php } else{ ?>
          <p><a href="/userprofilesearch/?id={{$forums->user_id}}"><?php echo $user['username']; ?></a></p>
        <?php } ?>

        </td>
        
        <td>
          <?php
            $page_id = get_page_id($forums->forum_id);
             $page_no=$page_id/4;    
           ?>
           <?php 
    if(preg_match('/'.$q.'/i', $forums->heading) == TRUE )
    {
      $title= "Post";
      $content= $forums->heading;
    }elseif(preg_match('/'.$q.'/i', $forums->name) == TRUE )
    {
      $title= "name"; 
      $content= $forums->name;
    }elseif(preg_match('/'.$q.'/i', $forums->description) == TRUE )
    {
      $title= "Description";
      $content= $forums->description;
    }elseif(preg_match('/'.$q.'/i', $forums->cmtName )== TRUE )
    {
      $title= "Comment Name";
      $content=  $forums->cmtName;
    }elseif(preg_match('/'.$q.'/i', $forums->comment )== TRUE )
    {
      $title= "Comment";
      $content=$forums->comment ;
    }else{
      $title= "";
      $content= "";
    }
         ?>
           <a href="/forumsearch?page=<?php echo ceil($page_no); ?>&id=<?php echo $page_id;?>" ><?php custom_echo($content, 100); ?> </a>
        </td>
        <td>
          {{$title}}
        </td>
        <td><?php echo date('M / j / Y g:i A', strtotime($time)); ?></td>
       
      </tr>
      <?php $sno++; ?>
    @endforeach
    @elseif (count($forum) <= 1)
 
 <br>  
</div>    
  <tr>
      <?php
      $lan_key= $_SESSION['key']; 
      $chk_forum=check_forum($lan_key); ?>
      @if (count($chk_forum) == 0)
      <td colspan="4">
          <h3 class="text-center" style="color: #337AB7;">Sorry!, No Posts Added in this Forum Add New Post.</h3></td>
      @else(count($chk_forum) >= 1)
      <div class="col-md-1 pull-left">
       <a href="{{ URL::previous() }}" class="btn btn-primary" ><i class="fa fa-reply">&nbsp;&nbsp;Back</i></a> 
  {{ Form::open( [ 'url' => 'forumsearch', 'class'=>'form-horizontal','method' => 'post', 'files' => true ] ) }}   
       <!--  <button class="btn btn-primary" ><i class="fa fa-reply">&nbsp;&nbsp;Back</i></button>  -->
        <!-- {{ URL::previous() }} -->
       <!--  <input type="hidden" name="edit" id="edit" value="edit">  -->           
  {!! Form::close() !!}
  <br>
      <td colspan="4">
          <h3 class="text-center" style="color: #337AB7;">Sorry!, Search term not found.</h3></td>
      </tr>
      @endif
  @endif    
 </tbody>
  </table>
  <?php if($sno > 4 ) { ?>
  <?php if (isset($search )) {
   echo $forum->appends(array('q' => $_SESSION['q']))->links(); 
  }else {?>
    {!! $forum->links() !!}
  <?php } ?> 
  <?php }else {} ?>
  <hr>
      </div>

  </div>


  </div> <!---Row-->
  <div class="col-md-2"></div>
 </div><!---Container-->


 @endsection
 <?php 
   function get_page_id($forum_id){
    $forum= \DB::table('forums')
                ->where('language_id', '=',$_SESSION['key'])
                ->get();
    $i=1;
    foreach ($forum as $key => $value) {
      
      if($value->id == $forum_id)
      {
        return $i;
      }else
      {
        $i++;
      }
      
    }
    return $i;
    //return $lan_forum;
  }

  function check_forum($lan_key){
    $lan_forum= \DB::table('forums')
                ->where('language_id', '=', $lan_key)
                ->get();
    return $lan_forum;
  }
  function username($id)
  {
    $single_comment= \DB::table('user_profile')
                ->where('user_id', '=', $id)
                ->get();
    foreach ($single_comment as $key => $value) {
      # code...
       $user= $value->userName;
       $id=$value->id;
    }
    $userdata = array('username' =>$user ,'id' =>$id );
    //print_r($userdata);
    return $userdata;   
  }
function readsegment()
    {         
    if(isset($_GET['page']))
    {
        $current_page=$_GET['page'];
    }else
    {
        $current_page=1;
    }
    $page=$current_page-1;
        return $page;
}
function get_single_comment($id){
  $single_comment= \DB::table('forum_comments')
                ->where('forum_id', '=', $id)
                ->get();
    return $single_comment;            
}
function get_comment($id){
$forum_comments= \DB::table('forum_comments')
                ->where('forum_id', '=', $id)
                ->get();
 return $forum_comments;
}
function custom_echo($x, $length)
{
  if(strlen($x)<=$length)
  {
    echo $x;
  }
  else
  {
    $y=substr($x,0,$length) . '...';
    echo $y;
  }
}
function get_post_userid($id){
  
  $forums= \DB::table('forums')
                ->where('id', '=', $id)
                ->get();
 foreach ($forums as $key => $value) {
   # code...
    $post_user_id=$value->user_id;
 }
 //echo $post_user_id; 
 return $post_user_id;
}
function get_comment_userid($id){
  $forums= \DB::table('forum_comments')
                ->where('id', '=', $id)
                ->get();
  foreach ($forums as $key => $value) {
    # code...
    $comment_user_id = $value->user_id;
  }
  return $comment_user_id;
}
function get_forum($id){
  $forum= \DB::table('forums')
                ->where('id', '=', $id)
                ->get();
  return $forum;
}
?>



<script type="text.javascript">
  function preventBack(){window.history.forward();}
  setTimeout("preventBack()", 0);
  window.onunload=function(){null};
</script>