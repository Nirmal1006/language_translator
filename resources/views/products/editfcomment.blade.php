@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row">
  <div class="panel panel-info">
          <div class="panel-heading">
              <h3 class="panel-title">Forum : <?php switch ($_SESSION['key']) {
            case 0:
                
                echo "German"; 
                break;
            case 1:
                
                echo "Frence"; 
                break;
            case 2:
               
                echo "Italian"; 
                break;
            case 3:
                
                echo "Spanish";
                break;
            default:
               
                echo "German- Default"; 
                break;
        } ?> </h3>
          </div>
  </div><!---Header Over-->

  <hr>
  @if ($errors->has())
  <div class="alert alert-danger">
      @foreach ($errors->all() as $error)
          {{ $error }}<br>        
      @endforeach
  </div>
  @endif
  <div class="col-md-12">
  
      <div class="table-responsive">  
      @foreach ($forum as $key=>$forums)        
        <table class="table table-bordered ">
        <tr>
        
        <th>
       <p>User Name:&nbsp;&nbsp;{{$forums->name}}</p>
       <p>Posted On:&nbsp;&nbsp;<?php echo date('M / j / Y g:i A', strtotime($time=$forums->created_at)); ?></p>
       </th>
       <th><p class="pull-right">Post No:&nbsp;&nbsp;1</p></th>
       </tr>
        <tbody>
    <tr>
      <td colspan="3">
        <span style="font-size:22px;">Description:</span><p style="font-size:18px;padding-left:5em;">{{$forums->description}}</p>
        <hr>
      </td>
    </tr>
    <tr>
    <td colspan="3" style="background-color:#f5f5f5;"><span style="font-size:22px;">Comments :</span><p style="font-size:16px;padding-left:5em;"></p>
    {!! Form::open(['url' => 'fpostcommentupdate', 'method'=>'post', 'class'=>'form-inline']) !!}
   <!--  <form class="form-inline" role="form"> -->
<?php foreach ($forum_comments as $key => $comment) {
	
	$cmt=$comment->comment;
	$cmtid=$comment->id;
} ?>
    <div class="form-group">
      <?php if(Auth::check() && isset($_SESSION['username'])) { ?>
      <input type="text" class="form-control" id="text" name= "name" value="<?php echo $_SESSION['username']; ?>" readonly>
      <input type="hidden" name="cmt_id" id="cmt_id" value="{{$cmtid}}">
      <?php }else{ ?>
       <input type="text" class="form-control" id="text" name= "name" placeholder="Name" required>
       
        <?php }
if(Auth::check() && isset($_SESSION['userid']) )
  {$user_id = $_SESSION['userid'];}else{$user_id=0;}
     ?>   


    <input type="hidden" name="id" id="id" value="{{$cmtid}}">
     <input type="hidden" name="user_id" id="user_id" value="<?php echo $user_id; ?>">
    </div>
    <div class="form-group">
    <?php if(Auth::check() && isset($_SESSION['username'])) { ?>
        <input type="email" name="email" id="email" class="form-control form-control-lg" value="<?php echo $_SESSION['useremail']; ?> " readonly>

      <?php  }else { ?> 
        <input type="email" name="email" id="email" class="form-control form-control-lg" placeholder="Email" >
      <?php } ?> 
     <!--  <input type="password" class="form-control" id="pwd" placeholder="Enter password"> -->
    </div>
        <div class="form-group">
      <textarea name="comment" required  name="editor1" class="form-control" rows="5" cols="50" placeholer="Message" >{{$cmt}}</textarea>
    </div>
    <button type="submit" class="btn btn-primary"><span class="fa fa-send">&nbsp;&nbsp;&nbsp;&nbsp;Update Comment</span></button>
    <a href="/forumsearch" class="btn btn-primary" data-toggle="modal">
                                <i class="fa fa-btn fa-reply"></i>&nbsp;&nbsp;Cancel
                                </a>
 <!--  </form> -->
<!-- <a class="pull-right" href="" class="fa fa-comment" aria-hidden="true">&nbsp;&nbsp;Post Comment</a> -->

  {!! Form::close() !!} 
    </td>
    </tr>
    </tbody>
        </table>
       @endforeach
       </div>
    </div>
  	</div><!-- Row -->
  </div><!-- Container -->



 @endsection