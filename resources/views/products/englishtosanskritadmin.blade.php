@extends('layouts.app')

@section('content')
<?php 
function readsegment()
    {         
    if(isset($_GET['page']))
    {
        $current_page=$_GET['page'];
    }else
    {
        $current_page=1;
    }
    $page=$current_page-1;
        return $page;
}


?>
<div class="container">
    <div class="row">
    
     <div class="panel panel-info">
          <div class="panel-heading">
             <h3 class="panel-title">Page : <?php switch ($_SESSION['key']) {
            case 0:
                
                echo "Tamil"; 
                break;
            case 1:
                
                echo "Telugu"; 
                break;
            case 2:
               
                echo "Malayalam"; 
                break;
            case 3:
                
                echo "Kanada";
                break;
            case 4:
                
                echo "Hindi";
                break;
            case 5:
                
                echo "Sanskrit";
                break;
            case 6:
                
                echo "Punjabi";
                break;
            case 7:
                
                echo "Marathi";
                break;
            case 8:
                
                echo "Bengali";
                break;

            default:
               
                echo "Tamil- Default"; 
                break;
        } ?> </h3>
          </div>
  </div><!---Header Over-->
        <div class="col-md-3"></div>
        <div class="col-md-6 text-center">
            {!! Form::open(['url' => 'show', 'method'=>'get', 'class'=>'form-inline']) !!}
            	<div class="row">
            	<div class="col-md-3"></div>
            	<div class="col-md-4">
                <div class="form-group {!! $errors->has('q') ? 'has-error' : '' !!}">
                 
                    {!! Form::text('q', isset($q) ? $q : null, ['class'=>'form-control','id'=>'transliterateTextareak', 'placeholder' => 'Enter the word to search']) !!}
                    {!! $errors->first('q', '<p class="help-block">:message</p>') !!}
                </div>
                </div>
                <div class="col-md-2">
                <form>
                   <button type="submit" class="btn fa fa-search btn-search">&nbsp;&nbsp;Search</button>
                   <br>
                    <a href="{{ route('products.create') }}" class="btn fa fa-plus btn-add">&nbsp;&nbsp;Add New</a><br>
                </form>
                </div>
                <div class="col-md-3"></div>
                </div>
            {!! Form::close() !!}    
          <hr>
          <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-10">
            <?php $seg=readsegment(); $sno=$seg*15+1; ?>
                <table class="table table-hover table-bordered table-responsive">
                                <thead>
                                    <tr>
                                        <th style="width: 20px;">S.No</th>
                                        <th >English</th>
                                        <th >Spanish</th>
                                        <th style="width: 150px;"><i class="fa fa-cog fa-1x" aria-hidden="true"></i></th>
                                    </tr>
                                </thead>
                                <tbody>
                                 @if (count($products) >= 1)
                                    @foreach($products as $index=>$product)
                                    <tr>
                                        <td style=" text-align: center;">{{ $sno}}</td>
                                        <td><a href="productdetails/?id={{ $product->user_id }}&rid={{$product->id}}">
                                        <?php echo ucwords($product->name); ?></a></td>
                                        <td><a href="productdetails/?id={{ $product->user_id }}&rid={{$product->id}}">
                                        <?php echo ucwords($product->model); ?></a></td>
                                         @if($_SESSION['userid'] === $product->user_id) 
                                            {!! Form::model($product, ['route' => ['products.destroy', $product], 'method' => 'delete', 'class' => 'form-inline'] ) !!}
                                            <a href="{{ route('products.edit', $product->id)}}" style="padding-right: 10px;" ><i class="fa fa-pencil" aria-hidden="true"></i>
                                            </a> 
                                            <!-- <a href="{{ route('products.destroy', $product->id)}}" class="js-submit-confirm"><i class="fa fa-trash" aria-hidden="true"></i>
                                            </a> -->
                                            {!! Form::close()!!}
                                        @elseif($_SESSION['userid'] === 1) 
                                            {!! Form::model($product, ['route' => ['products.destroy', $product], 'method' => 'delete', 'class' => 'form-inline'] ) !!}
                                            <a href="{{ route('products.edit', $product->id)}}" style="padding-right: 10px;" ><i class="fa fa-pencil" aria-hidden="true"></i>
                                            </a>
                                            
                                            <a href="{{ route('products.destroy', $product->id)}}" class="js-submit-confirm"><i class="fa fa-trash" aria-hidden="true"></i>
                                            </a>
                                          
                                            {!! Form::close()!!}
                                        @endif
                                        </td>
                                    </tr>
                        <?php $sno++; ?>
                                    @endforeach
                                    @elseif (count($products) <= 1)
                                    <tr>
	                            <td colspan="4">
	                                <h3 class="text-center" style="color: #337AB7;">Sorry!, Search term not found.</h3></td>
	                            </tr>
	                            @endif    
                                </tbody>
                            </table>
                    {!! $products->links() !!}
                </div>
            </div>
            <div class="col-md-1"></div>
        </div>       
    </div>
</div>
    <style type="text/css">
        th{
            width: 200px;
            text-align: center;
        }
        
        .btn-add{
    background-color: black;
border-color: black;
border-radius: 10px;
margin-top: 3px;

}
.btn-add:hover{
background-color: black;
}
.btn-search{
 background-image: linear-gradient(to bottom, #DD6A6A, #f83f3f);
    border-radius: 10px;
    width: 100px;
}
btn-success{
	background-color: black;
}
    </style>
@endsection


<script type="text/javascript" src="https://www.google.com/jsapi"></script>
   
<script type="text/javascript">
 
      // Load the Google Transliterate API
      google.load("elements", "1", {
            packages: "transliteration"
          });
 
      function onLoad() {
        var options = {
            sourceLanguage:
                google.elements.transliteration.LanguageCode.ENGLISH,
            destinationLanguage:
                [google.elements.transliteration.LanguageCode.SANSKRIT],
            shortcutKey: 'ctrl+g',
            // destinationLanguage:sanskrit
            //     [google.elements.transliteration.LanguageCode.KANNADA],
            // shortcutKey: 'ctrl+k',
            transliterationEnabled: false
        };
 
        // Create an instance on TransliterationControl with the required
        // options.
        var control =
            new google.elements.transliteration.TransliterationControl(options);
 
        // Enable transliteration in the textbox with id
        // 'transliterateTextarea'.
        control.makeTransliteratable(['transliterateTextareak']);
      }
      google.setOnLoadCallback(onLoad);
    </script>


