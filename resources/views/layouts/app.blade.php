﻿<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>English to German</title>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel='stylesheet' type='text/css'>
    <!-- Styles -->  
    <link href="{{ asset('public/css/app.css') }}" rel="stylesheet">

    <script type="text/javascript" src="{{ asset('public/js/squire-raw.js') }}"></script>

    <link href="{{ asset('public/css/site.css') }}" rel="stylesheet">

    <style>
        body {
            font-family: "Calibri",Calibri Light;
            font-size: 15px;
        }

        .fa-btn {
            margin-right: 6px;
        } .test{
            right:0;
            
           
            position:fixed;
            z-index: 1; 
        }
         
    </style>
</head>
<body id="app-layout" >
    <div class="container">
        <div class="header"></div>   
         <div class="col-md-1">
        </div>

        <div class="col-md-11">
            <nav class="navbar navbar-default" role="navigation">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                         @if(Auth::check())
                            <li><a href="{{ url('/products') }}" data-toggle="tooltip" title="English To Tamil">EN<->TA</a></li>
                            <li class="line">|</li>
                            <li><a href="{{ url('/englishtofrenchadmin') }}" data-toggle="tooltip" title="English To Telugu">EN<->TE</a></li>
                            <li class="line">|</li>
                            <li><a href="{{ url('/englishtoitalianadmin') }}"data-toggle="tooltip" title="English To Malayalam">EN<->ML</a></li>
                            <li class="line">|</li>
                            <li><a href="{{ url('/englishtospanishadmin') }}"data-toggle="tooltip" title="English To Kanada">EN<->KA</a></li>
                            <li class="line">|</li>
                            <li><a href="{{ url('/englishtohindhiadmin') }}"data-toggle="tooltip" title="English To Hindi">EN<->HD</a></li>
                            <li class="line">|</li>
                            <li><a href="{{ url('/englishtosanskritadmin') }}"data-toggle="tooltip" title="English To Sanskrit">EN<->SK</a></li>
                            <li class="line">|</li>
                            <li><a href="{{ url('/englishtopunjabadmin') }}"data-toggle="tooltip" title="English To Punjabi">EN<->PJ</a></li>
                            <li class="line">|</li>
                            <li><a href="{{ url('/englishtomarathiadmin') }}"data-toggle="tooltip" title="English To Marathi">EN<->MH</a></li>
                            <li class="line">|</li>
                            <li><a href="{{ url('/englishtobengaladmin') }}"data-toggle="tooltip" title="English To Bengali">EN<->BG</a></li>
                         @else
                            <li><a href="{{ url('/') }}" data-toggle="tooltip" title="English To Tamil">EN<->TA</a></li>
                            <li class="line">|</li>
                            <li><a href="{{ url('/englishtofrench') }}"data-toggle="tooltip" title="English To Telugu">EN<->TE</a></li>
                            <li class="line">|</li>
                            <li><a href="{{ url('/englishtoitalian') }}"data-toggle="tooltip" title="English To Malayalam ">EN<->ML</a></li>
                            <li class="line">|</li>
                            <li><a href="{{ url('/englishtospanish') }}"data-toggle="tooltip" title="English To Kanada">EN<->KA</a></li>
                            <li class="line">|</li>
                            <li><a href="{{ url('/englishtohindhi') }}"data-toggle="tooltip" title="English To Hindi">EN<->HD</a></li>
                            <li class="line">|</li>
                            <li><a href="{{ url('/englishtosanskrit') }}"data-toggle="tooltip" title="English To Sanskrit">EN<->SK</a></li>
                            <li class="line">|</li>
                            <li><a href="{{ url('/englishtopunjab') }}"data-toggle="tooltip" title="English To Punjabi">EN<->PJ</a></li>
                            <li class="line">|</li>
                            <li><a href="{{ url('/englishtomarathi') }}"data-toggle="tooltip" title="English To Marathi">EN<->MH</a></li>
                            <li class="line">|</li>
                            <li><a href="{{ url('/englishtobengal') }}"data-toggle="tooltip" title="English To Bengali">EN<->BG</a></li>
                        @endif
                            <div class="col-md-2 col-md-offset-2"></div>
                            @if(Auth::check())
                            <li class="next"><a href="{{ url('/products') }}">HOME</a></li>
                            @else
                            <li class="next"><a href="{{ url('/') }}">HOME</a></li>
                            @endif
                            <li><a href="{{ url('/forumsearch') }}">FORUM</a></li>
                            <li class="dropdown">
                                <a href="#" clas=u74="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    USER <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="{{ url('/profiles') }}"><i class="fa fa-btn fa-book"></i>&nbsp;&nbsp;PROFILES</a></li>
                                </ul>
                            </li>
                            <li><a href="{{ url('/aboutus') }}">ABOUT US</a></li>

                            @if (Auth::guest())
                            <li class="social pull-right"><a href="{{ url('/register') }}">REGISTER</a></li>
                            <li class="social pull-right"><a href="{{ url('/login') }}">LOGIN</a></li>
                            @else
                           
                            <li class="dropdown pull-right">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>
                            <?php  
                            Session::put('email', Auth::user()->email);
                            $_SESSION['username']= Auth::user()->name;
                                   $_SESSION['userid']= Auth::user()->id;
                                   $_SESSION['useremail']= Auth::user()->email; 
                                   $_SESSION['created_at']= Auth::user()->created_at;
                             ?>
                                <ul class="dropdown-menu" role="menu">
                                     @if(Auth::check() && Auth::user()->id === 1 )
                            <li ><a href="{{ url('/superadmin') }}" ><i class="fa fa-btn fa-cog"></i>Super Admin Settings</a></li>
                            @else
                            @endif
                            <li ><a  href="{{ url('/userregister') }}"><i class="fa fa-btn fa-user"></i>My Profile</a></li>
                                    <li><a href="{{ url('/usertest') }}"><i class="fa fa-btn fa-sign-out"></i>LOGOUT</a></li>
                                </ul>
                            </li>
                            @endif
                        </ul>
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
            </nav>

        </div>
    </div>    
     @if(isset($_GET['page']) ) 
     @else
       <div class="container"> 
       @if (Session::has('flash_notification.message'))
        <div class="col-sm-8"></div>
        <div class="col-sm-4 test">
            <div class="alert alert-{{ Session::get('flash_notification.level') }}">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                 {{ Session::get('flash_notification.message') }}
            </div>
        </div>       
    @endif
     @endif
    </div>   

    @yield('content')

    <span class="clearfix"></span>
    <div class="navbar navbar-default navbar-static-bottom">
        <div class="container">
            <p class="navbar-text col-md-offset-2">© 2016 - <a href="http://schanetz.com/">Schanetz</a>           
            </p>
            <span class=" navbar-text pull-right">Develop By - <a href="http://www.albericsoft.com/" >
                Alberich Software Pvt Ltd.</a></span> 
        </div>
    </div>
    <!-- JavaScripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script> -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    <script src="{{ asset('public/js/all.js') }}"></script>

<script>
    $('div.alert').not('.alert-important').delay(3000).fadeOut(350);
    //$('div.alert').delay(5000).slideUp(500);
</script>
    
        </body >
        </html>
