@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
	<div class="col-md-2"></div>
	<div class="col-md-8">
		<div class="panel panel-danger">
			 <div class="panel-heading"><h2> 404 - Error !!!</h2></div>
			  <div class="panel-body">
			    <h3 style="text-align:center">Oops ! Page Not Found </h3>
			    
			  </div>
		</div>
	</div>
	<div class="col-md-2"></div>
		<!-- <h2>404 Page Not Found Error !!!</h2> -->
	</div>
</div>

@endsection