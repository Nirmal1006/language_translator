@extends('layouts.app')

@section('content')
<?php 
 ?>
 <style>
	.img-style img{
		width: 50px;
		height:50px;
	}
</style>
<?php 

function readsegment()
    {         
    if(isset($_GET['page']))
    {
        $current_page=$_GET['page'];
    }else
    {
        $current_page=1;
    }
    $page=$current_page-1;
        return $page;
}

// if (isset($product)) {
//  	$product="active";
//  }elseif(isset($forum)){
//  	$product="active";
//  	} 
?>
<div class="container">
	<div class="row">
		<nav class="navbar navbar-default">
  <div class="container-fluid">
    <ul class="nav navbar-nav">
      <li class="<?php if (isset($product)) { echo "active";  }else {} ?>"><a  href="{{url('adminproduct')}}">Product</a></li>
      <li class="<?php if (isset($forums)) { echo "active";  }else {} ?>"><a href="{{url('adminforum')}}">Forum</a></li>
      <li class="<?php if (isset($users)) { echo "active";  }else {} ?>"><a href="{{url('adminuser')}}">User</a></li>
      <li class="<?php if (isset($tables)) { echo "active";  }else {} ?>"><a href="{{url('admintable')}}">Tables</a></li>
    </ul>
  </div>
</nav>
	</div>
</div>
<?php 
if (isset($users)) {	?>
<div class="container">
		<div class="row">
			<div class="panel panel-info">
				  <div class="panel-heading">
				    <h3 class="panel-title">User Profiles :</h3>
				  </div>
				  <div class="panel-body">
				    <div class="container">
						<div class="row">
							<div class="col-md-1"></div>
								<div class="col-md-10">
									<div class="row">
										<div class="col-md-6">	
										<table class="table table-hover">
									    <thead>
									      <tr>
									        <th>Name</th>
									        <th>Email</th>
									        <th>Image</th>
									        <th>Action </th>
									      </tr>
									    </thead>
								    
    <tbody>
    @foreach($users as $index=>$user)
    <a href="/userprofilesearch/?id={{$user->id}}">	
	      <tr>
	        <td><a href="/userprofilesearch/?id={{$user->user_id}}">{{$user->userName}}</a></td>
	        <td><a href="/userprofilesearch/?id={{$user->user_id}}">{{$user->email}}</a></td>
	        <td><?php if(isset($user)){ ?> 
				<div class="img-style" >
				<?php if($user->photo == '') {  ?>
					<a href="/userprofilesearch/?id={{$user->user_id}}">
				{!! Html::image('public/img/profile/profile.jpg') !!}
					</a>
				<?php }else{ ?>
					<a href="/userprofilesearch/?id={{$user->user_id}}">
				{!! Html::image('public/img/profile/'.$user->photo) !!}
					</a>
				<?php }
				?>
				</div>
				<?php }else {} ?></td>
				
				<td>
				{{ Form::open( [ 'url' => 'adminuserdelete', 'class'=>'form-horizontal','method' => 'post', 'files' => true ] ) }}
				<input type="hidden" name="user_id" id="user_id" value="{{$user->user_id}}">
				&nbsp;<a class="btn btn-ls btn-danger js-submit-confirm" style="margin-top:10px;"><i class="fa fa-trash" aria-hidden="true"></i></a>
				{!! Form::close() !!}
				</td>
				
	      </tr>
     </a>
    @endforeach
    </tbody>
  </table>
  {!! $users->links() !!}
										</div>
										<div class="col-md-6"></div>
									</div>
								</div>
							<div class="col-md-1"></div>
						</div>	    	
				    </div>
				  </div>
			</div>
		</div>
	</div>
	
 <?php }elseif(isset($product))
 { ?>
<div class="container">
		<div class="row">
			<div class="panel panel-info">
				  <div class="panel-heading">
				    <h3 class="panel-title">Product Details :</h3>
				  </div>
				  <div class="panel-body">
				    <div class="container">
						<div class="row">
							<div class="col-md-1"></div>
								<div class="col-md-10">
									<div class="row">
										<div class="col-md-2"></div>
            <div class="col-md-8">
            <?php $seg=readsegment(); $sno=$seg*15+1; ?>
                <table class="table table-hover table-bordered table-responsive">
                    <thead>
                        <tr>
                            <th style="width: 20px;">S.No</th>
                            <th >English</th>
                            <th >Other Language</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if (count($products) >= 1)
                        @foreach($products as $index=>$product)
                        <tr>
                            <td style=" text-align: center;">{{ $sno}}</td>
                            <td><a href="productdetails/?id={{ $product->user_id }}&rid={{$product->id}}"><?php echo ucwords($product->name); ?></a></td>
                                        <td><a href="productdetails/?id={{ $product->user_id }}&rid={{$product->id}}"><?php echo ucwords($product->model); ?></a></td>
                            <td>
                            {{ Form::open( [ 'url' => 'adminproductdelete', 'class'=>'form-horizontal','method' => 'post', 'files' => true ] ) }}
                            <input type="hidden" name="product_id" id="product_id" value="{{$product->id}}">
                            <a href="" class="btn btn-ls btn-danger js-submit-confirm"><i class="fa fa-trash" aria-hidden="true"></i></a>
                            {!! Form::close() !!}
                            </td>
                        </tr>
                        <?php $sno++; ?>
                        @endforeach
                        @elseif (count($products) <= 1)
                        <tr>
                            <td colspan="3">
                                <h3 class="text-center" style="color: #337AB7;">Sorry!, Search term not found.</h3></td>
                            </tr>
                            @endif                                           
                        </tbody>
                    </table>
                    {!! $products->links() !!}
                </div>
                <div class="col-md-2"></div>
									</div>
								</div>
							<div class="col-md-1"></div>
						</div>	    	
				    </div>
				  </div>
			</div>
		</div>
	</div>
 <?php 	}elseif(isset($forums)) { ?>
 <div class="container">
		<div class="row">
		<div class="col-md-1"></div>
		<div class="col-md-10">
			<div class="panel panel-info">
				  <div class="panel-heading">
				    <h3 class="panel-title">Forum :</h3>
				  </div>
				  <div class="panel-body">
				    
						<div class="row">
							<div class="col-md-1"></div>
							<div class="col-md-10">	
							<?php $seg=readsegment(); $sno=$seg*15+1; ?>
										<table class="table table-hover">
									    <thead>
									      <tr>
									        <th>Post No</th>
									        <th>Post Title</th>
									        <th>Posted By</th>
									        <th>Post Comment's</th>
									        <th>Action </th>
									      </tr>
									    </thead>
								    
									    <tbody>
									    @foreach($forums as $index=>$forum)
									    <tr>
									    	<td style=" text-align: center;">{{ $sno}}</td>
									    	<td>{{$forum->heading}}</td>
									    	<td>{{$forum->name}}</td>
									    	<td style=" text-align: center;"><?php echo $count_cmt=get_comment_count($forum->id);?></td>
									    	<td>
									    {{ Form::open( [ 'url' => 'adminforumdelete', 'class'=>'form-horizontal','method' => 'post', 'files' => true ] ) }}
                            <input type="hidden" name="forum_id" id="forum_id" value="{{$forum->id}}">
                            <a href="" class="btn btn-ls btn-danger js-submit-confirm"><i class="fa fa-trash" aria-hidden="true"></i></a>
                            {!! Form::close() !!}		
									    	</td>
									    </tr>
									    <?php $sno++; ?>
									    @endforeach
									    </tbody>
									    </table>
								{!! $forums->links() !!}
							</div>	
							<div class="col-md-1"></div>	
						</div>	    	
				   
				  </div>
			</div>
		  </div>
		<div class="col-md-1"></div>
		</div>
	</div>	
 	<?php }elseif(isset($tables)) { ?> 
<div class="container">
		<div class="row">
		<div class="col-md-1"></div>
		<div class="col-md-10">
			<div class="panel panel-info">
				  <div class="panel-heading">
				    <h3 class="panel-title">Table List's :</h3>
				  </div>
				  <div class="panel-body">
				    
						<div class="row">
							<div class="col-md-1"></div>
							<div class="col-md-10">	
		<table class="table table-hover">
			<thead>
      <tr>
        <th>Table Name </th>
        <th>Operation's</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Product Table</td>
        <td>
        {{ Form::open( [ 'url' => 'deleteproducttable', 'class'=>'form-horizontal','method' => 'post', 'files' => true ] ) }}
            
                            <a href="" class="btn btn-ls btn-danger js-submit-confirm"><i class="fa fa-trash" aria-hidden="true"></i></a>
                            <?php if(isset($admincontrol)){ ?> 
 
                            <a href="{{url('truncateproducttable')}}" class="btn btn-ls btn-warning"><i class="fa fa-refresh" aria-hidden="true"></i></a>
 
 <?php }else {} ?>
 {!! Form::close() !!}	
	
  </td>
      </tr>
      <tr>
								    <td>Forum Table</td>
									    <td>
 {{ Form::open( [ 'url' => 'deleteforumtable', 'class'=>'form-horizontal','method' => 'post', 'files' => true ] ) }}
                            <a href="" class="btn btn-ls btn-danger js-submit-confirm"><i class="fa fa-trash" aria-hidden="true"></i></a>
                           <?php if(isset($admincontrol)){ ?> 
 <a href="{{url('truncateforumtable')}}" class="btn btn-ls btn-warning js-submit-confirm"><i class="fa fa-refresh" aria-hidden="true"></i></a>
 							<?php }else {} ?>
 {!! Form::close() !!}	
	</td>
	</tr>
	<tr>
	 <td>Froum Comment's Table</td>
									    <td >
 {{ Form::open( [ 'url' => 'deleteforumcommenttable', 'class'=>'form-horizontal','method' => 'post', 'files' => true ] ) }}
                            <a href="" class="btn btn-ls btn-danger js-submit-confirm"><i class="fa fa-trash" aria-hidden="true"></i></a>
                            <?php if(isset($admincontrol)){ ?> 
                            <a href="{{url('truncateforumcommenttable')}}" class="btn btn-ls btn-warning js-submit-confirm"><i class="fa fa-refresh" aria-hidden="true"></i></a>
                            <?php }else {} ?>
 {!! Form::close() !!}	
	</td>
	</tr>
    <tr><td>User Profile Table</td><td>
 {{ Form::open( [ 'url' => 'deleteuserprofiletable', 'class'=>'form-horizontal','method' => 'post', 'files' => true ] ) }}
                            <a href="" class="btn btn-ls btn-danger js-submit-confirm"><i class="fa fa-trash" aria-hidden="true"></i></a>
                            <?php if(isset($admincontrol)){ ?> 
 <a href="{{url('truncateuserprofiletable')}}" class="btn btn-ls btn-warning js-submit-confirm"><i class="fa fa-refresh" aria-hidden="true"></i></a><?php }else {} ?>
 {!! Form::close() !!}	
	</td>
	</tr>
    </tbody>
						    
		</table>
  <!-- Trigger the modal with a button -->
  <?php if(isset($admincontrol)){  ?> 

  <?php }else { ?>
  <!--<button type="button" class="btn btn-info btn-sm pull-right" data-toggle="modal" data-target="#myModal">Login as Programmer</button>-->
  
  <?php }?>
 
  



							</div>	
							<div class="col-md-1"></div>	
						</div>	    	
				   
				  </div>
			</div>
		  </div>
		<div class="col-md-1"></div>
		</div>
	</div>
 	<?php }else {}  ?>
@endsection

<?php 
function get_comment_count($id){
	$single_comment= \DB::table('forum_comments')
                ->where('forum_id', '=', $id)
                ->count();
    return $single_comment;  
 }


?>


 <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Login Form</h4>
        </div>
        <div class="modal-body">
           {{ Form::open( [ 'url' => 'superadminlogin', 'class'=>'form-group','method' => 'post', 'files' => true ] ) }}
    <div class="form-group">
      <label for="email">Email:</label>
      <input type="email" class="form-control" name="email" id="email" placeholder="Enter email">
    </div>
    <div class="form-group">
      <label for="pwd">Password:</label>
      <input type="password" class="form-control" id="pwd" name="pwd" placeholder="Enter password">
    </div>
    <div class="checkbox">
      <label><input type="checkbox"> Remember me</label>
    </div>
    <button type="submit" class="btn btn-default">Submit</button>
  
   {!! Form::close() !!}	
        </div>
        
      </div>
      
    </div>
  </div>