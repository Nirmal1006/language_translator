@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div>

		  <!-- Nav tabs -->
		  <ul class="nav nav-tabs" role="tablist">
		    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Product </a></li>
		    <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Forum </a></li>
		    <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">User's</a></li>
		    <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Table's</a></li>
		  </ul>

		  <!-- Tab panes -->
		  <div class="tab-content">
		  <!-- Product Maintain Table -->
		    <div role="tabpanel" class="tab-pane active" id="home">...</div>
		    <!-- Forum Table -->
		    <div role="tabpanel" class="tab-pane" id="profile">...</div>
		    <!-- User's Profile Table -->
		    <div role="tabpanel" class="tab-pane" id="messages">...</div>
		    <!--  -->
		    <div role="tabpanel" class="tab-pane" id="settings">...</div>
		  </div>

		</div>

	</div>
</div>

@endsection