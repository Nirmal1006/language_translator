create view view_searchs as SELECT  forums.id, forums.language_id, forums.user_id, forums.name, forums.heading, forums.email, forums.description, forums.created_at, forums.updated_at, 
                      forum_comments.forum_id, 
                      forum_comments.user_id AS cmtUserId, forum_comments.name AS cmtName, 
                      forum_comments.email AS cmtEmail, forum_comments.comment, 
                      forum_comments.created_at AS cmtCreated, forum_comments.updated_at AS cmtUpdated
FROM         forums INNER JOIN
                      forum_comments ON forums.id = forum_comments.forum_id